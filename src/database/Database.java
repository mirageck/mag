package database;

import database.ColumNames.Names;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Database extends SQLiteOpenHelper {

	private static final String DOUBLE_TYPE = " REAL";
	private static final String COMMA_SEP = ",";
	private static final String SQL_CREATE_ENTRIES = "CREATE TABLE "
			+ Names.TABLE_NAME + " (" + Names._ID + " INTEGER PRIMARY KEY,"
			+ Names.COLUMN_NAME_LAT + DOUBLE_TYPE + COMMA_SEP
			+ Names.COLUMN_NAME_LNG + DOUBLE_TYPE +

			" )";

	public static final int DATABASE_VERSION = 1;
	public static final String DATABASE_NAME = "MyDatabes.db";

	public Database(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	public void onCreate(SQLiteDatabase db) {
		db.execSQL(SQL_CREATE_ENTRIES);
	}

	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

		onCreate(db);
	}

	public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		onUpgrade(db, oldVersion, newVersion);
	}
}
