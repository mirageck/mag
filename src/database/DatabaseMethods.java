package database;

import database.ColumNames.Names;
import parking.ParkingDatabase;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class DatabaseMethods {

	private int licznik;
	private Database base;


	// base.getWritableDatabase());

	public DatabaseMethods(Database base) {

		this.base = base;
		// TODO Auto-generated constructor stub
	}

	public int check(int id, Context context) {

		SQLiteDatabase db;

		db = base.getReadableDatabase();

		String selectQuery = "SELECT  * FROM " + Names.TABLE_NAME;

		Cursor cursor = db.rawQuery(selectQuery, null);
		cursor.moveToLast();

		int itemId = 0;
		System.out.println(itemId);

		try {

			itemId = cursor.getInt(cursor.getColumnIndexOrThrow(Names._ID));

		} catch (RuntimeException e1) {
			e1.printStackTrace();
			FileReader reader = new FileReader(context, base);
			reader.readFile(id);
			db = base.getReadableDatabase();
			cursor = db.rawQuery(selectQuery, null);
			cursor.moveToLast();
			itemId = cursor.getInt(cursor.getColumnIndexOrThrow(Names._ID));

		}
		cursor.close();
		System.out.println(itemId);
		licznik = itemId;

		return itemId;

	}

	public void addRecord(double lat, double lng) {
		SQLiteDatabase db;
		db = base.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(Names.COLUMN_NAME_LAT, lat);
		values.put(Names.COLUMN_NAME_LNG, lng);
		db.insert(Names.TABLE_NAME, null, values);

	}

	public double getLatitude(int id) {

		SQLiteDatabase db;

		db = base.getReadableDatabase();

		Cursor cursor = db.query(Names.TABLE_NAME, new String[] {
				Names.COLUMN_NAME_LAT, Names.COLUMN_NAME_LNG }, Names._ID
				+ "=?", new String[] { String.valueOf(id) }, null, null, null,
				null);
		if (cursor != null)
			cursor.moveToFirst();

		Double lat = Double.parseDouble(cursor.getString(0));
		cursor.close();
		// System.out.println("lta: " + Double.parseDouble(cursor.getString(0))
		// + " lng: " + Double.parseDouble(cursor.getString(1)));

		// TODO Auto-generated method stub
		return lat;
	}

	public double getLongitude(int id) {

		SQLiteDatabase db;

		db = base.getReadableDatabase();

		Cursor cursor = db.query(Names.TABLE_NAME, new String[] {
				Names.COLUMN_NAME_LAT, Names.COLUMN_NAME_LNG }, Names._ID
				+ "=?", new String[] { String.valueOf(id) }, null, null, null,
				null);
		if (cursor != null)
			cursor.moveToFirst();

		Double lng = Double.parseDouble(cursor.getString(1));
		cursor.close();

		// System.out.println("lta: " + Double.parseDouble(cursor.getString(0))
		// + " lng: " + Double.parseDouble(cursor.getString(1)));
		return lng;
	}

	public void loadAll(ParkingDatabase parkingDatabes) {
		// TODO Auto-generated method stub

		SQLiteDatabase db;

		db = base.getReadableDatabase();

		for (int i = 1; i < licznik; i++) {
			Cursor cursor = db.query(Names.TABLE_NAME, new String[] {
					Names.COLUMN_NAME_LAT, Names.COLUMN_NAME_LNG }, Names._ID
					+ "=?", new String[] { String.valueOf(i) }, null, null,
					null, null);
			if (cursor != null)
				cursor.moveToFirst();
			parkingDatabes.addParkingLocation(
					Double.parseDouble(cursor.getString(0)),
					Double.parseDouble(cursor.getString(1)));
			cursor.close();

		}
		System.out.println("załadowane");
	}
}
