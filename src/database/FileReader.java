package database;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Scanner;

import database.ColumNames.Names;



import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class FileReader {

	Context context;
	SQLiteDatabase db;

	public FileReader(Context context, Database base) {
		// TODO Auto-generated constructor stub

		this.context = context;

		db = base.getWritableDatabase();

	}

	public int readFile(int id) {

		InputStream in2 = context.getResources().openRawResource(id);

		Scanner br = new Scanner(new InputStreamReader(in2));

		int licznik = 0;
		while (br.hasNext()) {
			String strLine = br.nextLine();

			int i = strLine.indexOf(",");
			String slat = strLine.substring(0, i);
			String slng = strLine.substring(i + 1);
			double lat = 0;
			double lng = 0;

			lat = Double.parseDouble(slat);
			lng = Double.parseDouble(slng);

			ContentValues values = new ContentValues();
			values.put(Names.COLUMN_NAME_LAT, lat);
			values.put(Names.COLUMN_NAME_LNG, lng);
			db.insert(Names.TABLE_NAME, null, values);

			licznik++;

		}
		System.out.println(""+licznik);
		return licznik;

	}

}
