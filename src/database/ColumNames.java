package database;

import android.provider.BaseColumns;

public final class ColumNames {
	// To prevent someone from accidentally instantiating the contract class,
	// give it an empty constructor.
	public ColumNames() {
	}

	/* Inner class that defines the table contents */
	public static abstract class Names implements BaseColumns {
		public static final String TABLE_NAME = "coordinates";
		public static final String COLUMN_NAME_LAT = "lat";
		public static final String COLUMN_NAME_LNG = "lng";

	}
}
