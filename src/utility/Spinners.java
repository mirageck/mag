package utility;

import android.view.View;
import android.widget.Spinner;

public class Spinners {

	private Spinner spinnerHours;
	public Spinner getSpinnerHours() {
		return spinnerHours;
	}

	public void setSpinnerHours(Spinner spinnerHours) {
		this.spinnerHours = spinnerHours;
	}

	public Spinner getSpinnerMinutes() {
		return spinnerMinutes;
	}

	public void setSpinnerMinutes(Spinner spinnerMinutes) {
		this.spinnerMinutes = spinnerMinutes;
	}

	public Spinner getSpinnerUsers() {
		return spinnerUsers;
	}

	public void setSpinnerUsers(Spinner spinnerUsers) {
		this.spinnerUsers = spinnerUsers;
	}

	private Spinner spinnerMinutes;
	private Spinner spinnerUsers;
	
	public Spinners(View idHours, View idMinutes, View idUsers) {
		// TODO Auto-generated constructor stub
		
		spinnerHours = (Spinner) idHours;
		spinnerMinutes = (Spinner) idMinutes;
		spinnerUsers = (Spinner) idUsers;
	}

	
}
