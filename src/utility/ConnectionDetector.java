package utility;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;


public class ConnectionDetector {

	private Context context;
	Boolean isInternetPresent = false;
	// Alert Dialog Manager
	Alert alert = new Alert();

	public ConnectionDetector(Context context) {
		this.context = context;
		alert.setContext(context);

	}

	public boolean checkInternet() {

		isInternetPresent = isConnectingToInternet();
		if (!isInternetPresent) {

			alert.showAlertDialog2(
					context,
					"Internet Error",
					"Brak połaczenia z internetem! Potrzebne do działania programu!",
					false);

			return isInternetPresent;
		} else {
			return isInternetPresent;
		}
	}

	public boolean isConnectingToInternet() {
		ConnectivityManager connectivity = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity != null) {
			NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if (info != null)
				for (int i = 0; i < info.length; i++)
					if (info[i].getState() == NetworkInfo.State.CONNECTED) {
						return true;
					}

		}
		return false;
	}
}
