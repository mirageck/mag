package utility;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import android.os.AsyncTask;
import android.util.Log;

public class GettingUrlTask extends AsyncTask<String, Void, String> {

	public GettingUrlTask() {

	}

	@Override
	protected String doInBackground(String... url) {

		String data = "";

		try {

			data = downloadUrl(url[0]);
		} catch (Exception e) {
			Log.d("Background Task", e.toString());
		}
		return data;
	}

	@Override
	protected void onPostExecute(String result) {
		
		System.out.println("skonczyłem");
		super.onPostExecute(result);

	}

	/** A method to download json data from url */
	private String downloadUrl(String strUrl) throws IOException {

		String data = "";
		InputStream iStream = null;
		HttpURLConnection urlConnection = null;
		try {

			URL url = new URL(strUrl);

			try {

				urlConnection = (HttpURLConnection) url.openConnection();

			} catch (IOException e1) {

				e1.printStackTrace();
			}

			urlConnection.connect();

			// Reading data from url
			iStream = urlConnection.getInputStream();

			BufferedReader br = new BufferedReader(new InputStreamReader(
					iStream));

			StringBuffer sb = new StringBuffer();

			String line = "";
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}

			data = sb.toString();

			br.close();

		} catch (Exception e) {
			Log.d("Exception while downloading url", e.toString());
		} finally {
			iStream.close();
			urlConnection.disconnect();
		}
		return data;
	}

}
