package utility;

import java.util.ArrayList;
import java.util.List;
import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class SpinnersMethods {

	public SpinnersMethods(Context context, int simpleSpinnerItem,
			Spinners spinners) {

		List<Integer> listH = new ArrayList<Integer>();

		for (int i = 0; i < 24; i++) {
			listH.add(i);
		}

		List<Integer> listMin = new ArrayList<Integer>();

		listMin.add(0);
		for (int i = 59; i > 0; i--) {
			listMin.add(i);
		}

		List<Integer> listPlaceListNumber = new ArrayList<Integer>();

		for (int i = 1; i < 6; i++) {
			listPlaceListNumber.add(i);
		}

		ArrayAdapter<Integer> spinerAdapter1 = new ArrayAdapter<Integer>(
				context, simpleSpinnerItem, listH);

		spinerAdapter1
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		spinners.getSpinnerHours().setAdapter(spinerAdapter1);

		ArrayAdapter<Integer> spinerAdapter2 = new ArrayAdapter<Integer>(
				context, simpleSpinnerItem, listMin);

		spinerAdapter2
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinners.getSpinnerMinutes().setAdapter(spinerAdapter2);

		ArrayAdapter<Integer> spinerAdapter3 = new ArrayAdapter<Integer>(
				context, simpleSpinnerItem, listPlaceListNumber);

		spinerAdapter3
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		spinners.getSpinnerUsers().setAdapter(spinerAdapter3);

		spinners.getSpinnerHours().setOnItemSelectedListener(
				new AdapterView.OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> parent,
							View view, int position, long id) {
						// TODO Auto-generated method stub

						((TextView) parent.getChildAt(0))
								.setTextColor(Color.WHITE);
						((TextView) parent.getChildAt(0))
								.setGravity(Gravity.RIGHT);
					}

					@Override
					public void onNothingSelected(AdapterView<?> parent) {
						// TODO Auto-generated method stub

					}
				});

		spinners.getSpinnerMinutes().setOnItemSelectedListener(
				new AdapterView.OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> parent,
							View view, int position, long id) {
						// TODO Auto-generated method stub

						((TextView) parent.getChildAt(0))
								.setTextColor(Color.WHITE);
						((TextView) parent.getChildAt(0))
								.setGravity(Gravity.RIGHT);
					}

					@Override
					public void onNothingSelected(AdapterView<?> parent) {
						// TODO Auto-generated method stub

					}
				});

	}
}
