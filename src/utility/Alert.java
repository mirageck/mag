package utility;

import geocoding.GeocodingTask;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.Settings;

public class Alert {

	private Context myContext;

	public void setContext(Context con) {
		this.myContext = con;

	}

	public void showAlertDialog(Context context, String title, String message,
			Boolean status) {

		AlertDialog.Builder builder = new AlertDialog.Builder(context);

		builder.setTitle(title);

		builder.setMessage(message);

		if (status != null)

			builder.setNegativeButton("OK",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {

						}
					});

		builder.show();
	}

	public void showAlertDialog2(Context context, String title, String message,
			Boolean status) {

		AlertDialog.Builder builder = new AlertDialog.Builder(context);

		builder.setTitle(title);

		builder.setMessage(message);

		if (status != null)

			builder.setNegativeButton("OK",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {

						}
					});

		builder.setNegativeButton("Ustawienia Dane Pakietowe",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {

						Intent intent2 = new Intent(
								Settings.ACTION_WIRELESS_SETTINGS);
						intent2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						myContext.startActivity(intent2);

					}
				});

		builder.setNeutralButton("Ustawienia WIFI",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {

						Intent intent2 = new Intent(
								Settings.ACTION_WIFI_SETTINGS);
						intent2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						myContext.startActivity(intent2);

					}
				});

		builder.show();
	}

	
	
	public void showFinishDialog(final Context context, String title,
			String message, Boolean status  ) {

		AlertDialog.Builder builder = new AlertDialog.Builder(context);

		builder.setTitle(title);

		builder.setMessage(message);

		if (status != null)


		builder.setNeutralButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {

			}
		});

		builder.show();

	}
	
	
	
	public void showAlertDialog3(final Context context, String title,
			String message, Boolean status, final GeocodingTask geoTask) {

		AlertDialog.Builder builder = new AlertDialog.Builder(context);

		builder.setTitle(title);

		builder.setMessage(message);

		if (status != null)

			builder.setNegativeButton("Tak",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
						
							if (context instanceof Activity) {
								((Activity) context).finish();
							}
						}
					});

		builder.setNeutralButton("Nie", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {

			}
		});

		builder.show();

	}
}
