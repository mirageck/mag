package geocoding;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GeoJSONParser {


	
	public GeoJSONResult parse(JSONObject jObject) {

		List<HashMap<String, Double>> resultDouble = new ArrayList<HashMap<String, Double>>();
		List<HashMap<String, String>> resultString = new ArrayList<HashMap<String, String>>();

		GeoJSONResult result = new GeoJSONResult();
		JSONArray jResult = null;

		try {

			jResult = jObject.getJSONArray("results");

			System.out.println(jResult.length());

			for (int i = 0; i < jResult.length(); i++) {

				/** Traversing all legs */

				String adress;

				adress = (String) ((JSONObject) jResult.get(i))
						.get("formatted_address");

				HashMap<String, String> hashAdress = new HashMap<String, String>();
				hashAdress.put("Adress", adress);

				resultString.add(hashAdress);

				double lat = 0;
				double lng = 0;

				try {
					lat = (Double) ((JSONObject) ((JSONObject) ((JSONObject) jResult
							.get(i)).get("geometry")).get("location"))
							.get("lat");
				} catch (JSONException e1) {

					e1.printStackTrace();
				}

				lng = (Double) ((JSONObject) ((JSONObject) ((JSONObject) jResult
						.get(i)).get("geometry")).get("location")).get("lng");

				HashMap<String, Double> hashLat = new HashMap<String, Double>();

				hashLat.put("Lat", lat);

				HashMap<String, Double> hashLng = new HashMap<String, Double>();

				hashLng.put("Lng", lng);

				resultDouble.add(hashLat);
				resultDouble.add(hashLng);

				// System.out.println(adress);
				// System.out.println(lat);
				// System.out.println(lng);

			}

			result.set(resultDouble, resultString);
		} catch (Exception e) {
		}
		return result;
	}

}
