package geocoding;



import place.AddPlaceAdapter;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;

public class ServiceMethods {

	private Messenger mService = null;
	private boolean mIsBound;
	private Messenger mMessenger;
	Activity context;

	
	public ServiceMethods(Activity context, AddPlaceAdapter addPlaceAdapter) {
		mMessenger = new Messenger(new IncomingHandler(addPlaceAdapter));
		this.context = context;

		
	}


	public void doUnbindService() {
		if (mIsBound) {

			if (mService != null) {
				try {
					Message msg = Message.obtain(null,
							MyService.MSG_UNREGISTER_CLIENT);
					msg.replyTo = mMessenger;
					mService.send(msg);
				} catch (RemoteException e) {

				}
			}

			context.unbindService(mConnection);
			mIsBound = false;

		}
	}

	public void sendMessageToService(String name, int op) {
		if (mIsBound) {
			if (mService != null) {
				try {

					if(op==0){
					Bundle b = new Bundle();
					b.putString("name", name);
					Message msg = Message.obtain(null, MyService.MSG_NAME);
					msg.setData(b);

					msg.replyTo = mMessenger;
					mService.send(msg);
					}else{
						Bundle b = new Bundle();
						b.putString("city", name);
						Message msg = Message.obtain(null, MyService.MSG_CITY);
						msg.setData(b);

						msg.replyTo = mMessenger;
						mService.send(msg);
						
					}
				} catch (RemoteException e) {
				}
			}
		}
	}

	private ServiceConnection mConnection = new ServiceConnection() {
		public void onServiceConnected(ComponentName className, IBinder service) {
			mService = new Messenger(service);

			try {
				Message msg = Message.obtain(null,
						MyService.MSG_REGISTER_CLIENT);
				msg.replyTo = mMessenger;
				mService.send(msg);
			} catch (RemoteException e) {

			}
		}

		public void onServiceDisconnected(ComponentName className) {
			mService = null;

		}
	};

	public void doBindService() {
		context.bindService(new Intent(context, MyService.class), mConnection,
				Context.BIND_AUTO_CREATE);
		mIsBound = true;

	}



}
