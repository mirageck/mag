package geocoding;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class GeoJSONResult {

	private List<HashMap<String, Double>> resultDouble = new ArrayList<HashMap<String, Double>>();
	private List<HashMap<String, String>> resultString = new ArrayList<HashMap<String, String>>();

	public List<HashMap<String, Double>> getResultDouble() {
		return resultDouble;
	}

	public void setResultDouble(List<HashMap<String, Double>> resultDouble) {
		this.resultDouble = resultDouble;
	}

	public List<HashMap<String, String>> getResultString() {
		return resultString;
	}

	public void setResultString(List<HashMap<String, String>> resultString) {
		this.resultString = resultString;
	}

	public void set(List<HashMap<String, Double>> resultDouble,
			List<HashMap<String, String>> resultString) {

		this.resultDouble = resultDouble;
		this.resultString = resultString;

	}

}
