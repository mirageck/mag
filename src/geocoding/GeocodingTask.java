package geocoding;

import java.io.IOException;
import java.util.List;

import place.AddPlaceAdapter;
import utility.ConnectionDetector;
import android.app.Activity;
import android.content.Context;
import android.location.Address;
import android.os.AsyncTask;

public class GeocodingTask extends
		AsyncTask<List<Address>, Object, List<Address>> {

	Context context;
	ConnectionDetector cd;
	MyGeocode mc;
	String name;
	Address address = null;
	AddPlaceAdapter simpleAddAdpt;

	public GeocodingTask(String string, Context c,
			AddPlaceAdapter simpleAddAdpt, String city, Activity activity) {

		context = c;
		this.simpleAddAdpt = simpleAddAdpt;
		cd = new ConnectionDetector(context);
		mc = new MyGeocode(city);

		this.name = string;

	}

	@Override
	protected void onPreExecute() {

	}

	protected void onPostExecute(List<Address> result) {

		for (int i = 0; i < result.size(); i++) {
			simpleAddAdpt.add(result.get(i));
		}
	}

	@Override
	protected List<Address> doInBackground(List<Address>... params) {

		try {
			params[0] = mc.getCoordinatesList(name, context);

		} catch (IOException e) {
			e.printStackTrace();
		}

		// params[0] = mc.find(name);

		return params[0];
	}
}
