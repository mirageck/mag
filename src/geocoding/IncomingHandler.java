package geocoding;

import java.util.Locale;

import place.AddPlaceAdapter;


import android.location.Address;
import android.os.Handler;
import android.os.Message;

class IncomingHandler extends Handler {

	AddPlaceAdapter addPlaceAdapter;
	boolean option = true;

	public IncomingHandler(AddPlaceAdapter addPlaceAdapter) {
		// TODO Auto-generated constructor stub
		this.addPlaceAdapter = addPlaceAdapter;

	}

	@Override
	public void handleMessage(Message msg) {

		String adr = "";
		Double lat = 0.0;
		Double lng = 0.0;

		switch (msg.what) {

		case MyService.MSG_SIZE:
			int size = msg.arg1;
			System.out.println("dosta�em adress " + size);
			break;

		case MyService.MSG_ADDRESS:
			adr = msg.getData().getString("address");
			System.out.println("dosta�em adress " + adr);
			lat = msg.getData().getDouble("lat");
			System.out.println("lat " + lat);
			lng = msg.getData().getDouble("lng");
			System.out.println("lng " + lng);
			break;

		default:
			super.handleMessage(msg);
		}

		Address tmp = new Address(Locale.getDefault());
		tmp.setAddressLine(0, adr);
		tmp.setLatitude(lat);
		tmp.setLongitude(lng);

		addPlaceAdapter.clear();
		addPlaceAdapter.add(tmp);

	}
}
