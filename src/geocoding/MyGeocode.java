package geocoding;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import utility.Alert;
import utility.GettingUrlTask;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;

public class MyGeocode {

	Alert alert = new Alert();
	String city;
	GettingUrlTask downloadTask;
	GeoParserTask parserTask;

	public MyGeocode(String tmp) {
		city = tmp;

	}

	@SuppressWarnings("static-access")
	public Address getCoordinates(String name, Context context)
			throws IOException {

		Geocoder gc = new Geocoder(context);

		if (gc.isPresent()) {
			List<Address> list = gc.getFromLocationName(name + ", " + city, 1);
			Address address = list.get(0);
			return address;

		} else {
			return null;

		}

	}

	@SuppressWarnings("static-access")
	public List<Address> getCoordinatesList(String name, Context context)
			throws IOException {

		Geocoder gc = new Geocoder(context);

		if (gc.isPresent()) {
			List<Address> list = new ArrayList<Address>();
			try {

				list = gc.getFromLocationName(name + ", " + city, 3);

			} catch (java.io.IOException e1) {

				e1.printStackTrace();
			}

			return list;
		} else {
			return null;

		}
	}

	public List<Address> find(String name) {

		String url = getUrl(name);

		System.out.println(url);
		downloadTask = new GettingUrlTask();
		downloadTask.execute(url);
		parserTask = new GeoParserTask();

		try {
			parserTask.doInBackground(downloadTask.get());
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		} catch (ExecutionException e1) {

			e1.printStackTrace();
		}

		return parserTask.getList();

	}

	private String getUrl(String name) {

		name = name.replace(" ", "+");

		String url = "https://maps.googleapis.com/maps/api/geocode/json?address="
				+ name
				+ ","
				+ city;
			//	+ "&key=AIzaSyC2l3iIS1fpeoRxnnF1bNcX1RyJTdgh8nY";

		return url;
	}

}
