package geocoding;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.json.JSONObject;

import android.location.Address;

import com.google.android.gms.maps.model.LatLng;

public class GeoParserTask {

	private List<Address> list = new ArrayList<Address>();

	public List<Address> getList() {
		return list;
	}

	public GeoParserTask() {

	}

	public boolean doInBackground(String jsonData) {

		JSONObject jObject;
		GeoJSONResult result = null;

		try {
			jObject = new JSONObject(jsonData);
			GeoJSONParser parser = new GeoJSONParser();

			// Starts parsing data
			result = parser.parse(jObject);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (result.getResultString().size() < 1) {
			System.out.println("No Points");
			return false;
		}

		List<HashMap<String, Double>> pathDouble = result.getResultDouble();
		List<HashMap<String, String>> pathString = result.getResultString();

		ArrayList<String> adressList = new ArrayList<String>();

		List<Double> listLat = new ArrayList<Double>();
		List<Double> listLng = new ArrayList<Double>();

		ArrayList<LatLng> location = new ArrayList<LatLng>();

		for (HashMap<String, Double> x : pathDouble) {

			double nLat;
			double nLng;
			try {

				nLat = x.get("Lat");

				listLat.add(nLat);

			} catch (NullPointerException e1) {

				e1.printStackTrace();
			}

			try {

				nLng = x.get("Lng");
				listLng.add(nLng);

			} catch (NullPointerException e1) {

				e1.printStackTrace();
			}

		}

		for (HashMap<String, String> x : pathString) {

			String tmpAdress = x.get("Adress");

			try {

				if (tmpAdress.length() > 1) {

					adressList.add(tmpAdress);
				}

			} catch (NullPointerException e1) {

				e1.printStackTrace();
			}

		}

		for (int j = 0; j < listLat.size(); j++) {

			LatLng nPosition = new LatLng(listLat.get(j), listLng.get(j));
			location.add(nPosition);

			Address tmp = new Address(Locale.getDefault());
			tmp.setAddressLine(0, adressList.get(j));
			tmp.setLatitude(listLat.get(j));
			tmp.setLongitude(listLng.get(j));

			list.add(tmp);

		}

		System.out.println("adres" + adressList.size());
		System.out.println("lat" + listLat.size());
		System.out.println("lng" + listLng.size());
		System.out.println("Adresses" + list.size());

		System.out.println("Adresses:" + adressList.get(0));
		return true;
	}

}
