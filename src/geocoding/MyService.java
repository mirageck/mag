package geocoding;

import java.util.ArrayList;
import java.util.List;

import android.app.Service;
import android.content.Intent;
import android.location.Address;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.widget.Toast;

public class MyService extends Service {

	private Looper mServiceLooper;
	private ServiceHandler mServiceHandler;
	private List<Address> list = new ArrayList<Address>();
	private String city = "Kielce";

	public static final String LAT = "lat";
	public static final String LNG = "lng";
	String name = "";

	ArrayList<Messenger> mClients = new ArrayList<Messenger>();
	int mValue = 0;
	static final int MSG_REGISTER_CLIENT = 1;
	static final int MSG_UNREGISTER_CLIENT = 2;
	static final int MSG_ADDRESS = 3;
	static final int MSG_NAME = 4;
	static final int MSG_SIZE = 5;
	static final int MSG_LAT = 6;
	static final int MSG_LNG = 7;
	static final int MSG_CITY = 8;
	boolean calculate = false;

	final Messenger mMessenger = new Messenger(new IncomingHandler());

	class IncomingHandler extends Handler {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {

			case MSG_CITY:
				city = msg.getData().getString("city");
				break;

			case MSG_REGISTER_CLIENT:
				mClients.add(msg.replyTo);
				break;
			case MSG_UNREGISTER_CLIENT:
				mClients.remove(msg.replyTo);
				break;
			case MSG_NAME:
				name = msg.getData().getString("name");
				calculate = true;
				System.out.println("otrzymałem: " + name);
				break;
			default:
				super.handleMessage(msg);
			}
		}
	}

	private void sendMessageToUI(List<Address> list) {
		for (int i = mClients.size() - 1; i >= 0; i--) {

			try {

				mClients.get(i).send(
						Message.obtain(null, MSG_SIZE, list.size(), 0));

				Bundle b = new Bundle();
				b.putString("address", list.get(0).getAddressLine(0));
				b.putDouble("lat", list.get(0).getLatitude());
				b.putDouble("lng", list.get(0).getLongitude());
				Message msg = Message.obtain(null, MSG_ADDRESS);
				msg.setData(b);
				mClients.get(i).send(msg);

			} catch (RemoteException e) {

				mClients.remove(i);
			}
			// }
		}
	}

	// Handler that receives messages from the thread
	public final class ServiceHandler extends Handler {
		public ServiceHandler(Looper looper) {
			super(looper);
		}

		@Override
		public void handleMessage(Message msg) {

			while (true) {

				if (calculate) {
					MyGeocode mc = new MyGeocode(city);
					if (name.length() > 0) {
						list = mc.find(name);

						calculate = false;

						if (list.size() > 0) {
							sendMessageToUI(list);
						}
					}

				}
			}
		}
	}



	@Override
	public void onCreate() {

		HandlerThread thread = new HandlerThread("ServiceStartArguments");
		thread.start();

		mServiceLooper = thread.getLooper();
		mServiceHandler = new ServiceHandler(mServiceLooper);

	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		//Toast.makeText(this, "service starting", Toast.LENGTH_SHORT).show();

		Message msg = mServiceHandler.obtainMessage();
		msg.arg1 = startId;
		mServiceHandler.sendMessage(msg);

		return START_STICKY;
	}

	@Override
	public IBinder onBind(Intent intent) {
		// We don't provide binding, so return null
		return mMessenger.getBinder();

	}

	@Override
	public void onDestroy() {
		//Toast.makeText(this, "service done", Toast.LENGTH_SHORT).show();
		  super.onDestroy();
	}
}