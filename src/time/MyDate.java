package time;

import java.util.Calendar;
import java.util.Date;

public class MyDate {

	private Date timeStart;
	private Date timeEnd;
	private int h;
	private int min;
	private String name = "";

	public MyDate(Date time_start, int h, int min) {
		super();
		this.timeStart = time_start;

		Calendar cal = Calendar.getInstance();
		cal.setTime(time_start);

		cal.add(Calendar.HOUR, +h);
		timeEnd = cal.getTime();

		cal.add(Calendar.MINUTE, +min);
		timeEnd = cal.getTime();
		this.h = h;
		this.min = min;

	}

	public void setTime(Date start_time, String duration) {
		timeStart = start_time;

		Calendar cal = Calendar.getInstance();
		cal.setTime(timeStart);

		String min = "min";

		String hours = "h";

		duration.replace(" ", "");

		if (duration.contains(hours)) {

			int i_start = duration.indexOf(hours);
			int i_end;
			if (i_start > 2) {
				i_end = i_start - 2;
			} else {
				i_end = i_start - 1;
			}

			String tmp = duration.substring(i_end, i_start);

			int x = Integer.parseInt(tmp);

			cal.add(Calendar.HOUR, +x);
			timeEnd = cal.getTime();

		}

		if (duration.contains(min)) {

			int i_start = duration.indexOf(min);
			int i_end;
			if (i_start > 2) {
				i_end = i_start - 2;
			} else {
				i_end = i_start - 1;
			}

			String tmp = duration.substring(i_end, i_start);
			int x = Integer.parseInt(tmp);

			cal.add(Calendar.MINUTE, +x);
			timeEnd = cal.getTime();

		}

	}

	public void changeTime(int h, int min) {

		Date tmp;
		Calendar cal = Calendar.getInstance();
		cal.setTime(timeStart);

		cal.add(Calendar.HOUR, +h);
		tmp = cal.getTime();

		cal.add(Calendar.MINUTE, +min);
		tmp = cal.getTime();

		timeStart = tmp;

		cal.setTime(timeEnd);

		cal.add(Calendar.HOUR, +h);
		tmp = cal.getTime();

		cal.add(Calendar.MINUTE, +min);
		tmp = cal.getTime();

		timeEnd = tmp;
	}

	public void changeStartTime(Date start_time) {
		timeStart = start_time;
		Calendar cal = Calendar.getInstance();
		cal.setTime(timeStart);

		cal.add(Calendar.HOUR, +h);
		timeEnd = cal.getTime();

		cal.add(Calendar.MINUTE, +min);
		timeEnd = cal.getTime();

	}

	public void setTime(Date start_time, int h, int min) {
		timeStart = start_time;
		this.h = h;
		this.min = min;

		Calendar cal = Calendar.getInstance();
		cal.setTime(timeStart);

		cal.add(Calendar.HOUR, +h);
		timeEnd = cal.getTime();

		cal.add(Calendar.MINUTE, +min);
		timeEnd = cal.getTime();

	}

	public boolean ifNotTooLate(Date tmp_start) {

		if (timeStart.after(tmp_start)) {

			Calendar cal = Calendar.getInstance();
			cal.setTime(tmp_start);

			cal.add(Calendar.HOUR, +1);
			Date time_tmp = cal.getTime();

			return timeStart.before(time_tmp);
		} else {
			return false;
		}
	}

	public void setName(String name2) {
		// TODO Auto-generated method stub
		this.name = name2;

	}

	public String getName() {
		// TODO Auto-generated method stub
		return name;
	}

	public Date getTimeStart() {
		return timeStart;
	}

	public void setTimeStart(Date time_start) {
		this.timeStart = time_start;
	}

	public Date getTimeEnd() {
		return timeEnd;
	}

	public void setTimeEnd(Date time_end) {
		this.timeEnd = time_end;
	}

	public int getH() {
		return h;
	}

	public void setH(int h) {
		this.h = h;
	}

	public int getMin() {
		return min;
	}

	public void setMin(int min) {
		this.min = min;
	}

}
