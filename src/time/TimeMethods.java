package time;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class TimeMethods {

	public String getStringTime(int hours, int mins) {
		String tmp = "";

		if (hours != 0) {

			tmp = tmp + hours + "h ";
		}
		if (mins != 0) {

			tmp = tmp + mins + "min ";
		}
		return tmp;
	}

	public int calculateX(Date date, int startHour) {
		int tmpHours = getHoursFromDate(date);
		int tmpMin = getMinutesFromDate(date);

		tmpHours = tmpHours - startHour;

		if (tmpHours < 0) {
			// tmpHours = 24 - tmpHours;
			int tmp = 24 - startHour;
			tmpHours = getHoursFromDate(date) + tmp;
		}

		int loc2 = (tmpHours * 60) + (tmpMin);
		loc2 = loc2 * 2;
		return loc2;
	}

	public int getHoursFromDate(Date date) {
		Calendar calendar = GregorianCalendar.getInstance();

		calendar.setTime(date);

		return calendar.get(Calendar.HOUR_OF_DAY);
	}

	public int getMinutesFromDate(Date date) {
		Calendar calendar = GregorianCalendar.getInstance(); // creates a

		calendar.setTime(date);
		return calendar.get(Calendar.MINUTE);
	}

	public boolean comparateDate(Date date1, Date date2) {

		Calendar calendar = GregorianCalendar.getInstance();

		calendar.setTime(date1);

		Calendar calendar2 = GregorianCalendar.getInstance();

		calendar2.setTime(date2);

		if (calendar.get(Calendar.HOUR) == calendar2.get(Calendar.HOUR)) {

			if (calendar.get(Calendar.MINUTE) == calendar2.get(Calendar.MINUTE)) {
				return true;
			}

		}

		// TODO Auto-generated method stub
		return false;
	}

}
