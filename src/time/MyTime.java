package time;

public class MyTime {

	private int hours;
	private int minutes;

	public MyTime(int hours, int minutes) {
		super();
		this.hours = hours;
		this.minutes = minutes;
	}

	public MyTime() {
		this.hours = 0;
		this.minutes = 0;
		// TODO Auto-generated constructor stub
	}

	public int getHours() {
		return hours;
	}

	public void setHours(int hours) {
		this.hours = hours;
	}

	public int getMinutes() {
		return minutes;
	}

	public void setMinutes(int minutes) {
		this.minutes = minutes;
	}

}
