package place;

import java.util.List;

import android.content.Context;
import android.location.Address;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mag.R;

public class AddPlaceAdapter extends ArrayAdapter<Address> {

	private List<Address> addressList;
	private Context context;

	public AddPlaceAdapter(List<Address> addressList, Context ctx) {
		super(ctx, R.layout.add_row_layout, addressList);
		this.addressList = addressList;
		this.context = ctx;
	}

	public View getView(int position, View convertView, ViewGroup parent) {

		if (convertView == null) {

			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.add_row_layout, parent,
					false);
		}

		TextView tv1 = (TextView) convertView.findViewById(R.id.name_add1);
		TextView tv2 = (TextView) convertView.findViewById(R.id.name_add2);
		Address p = addressList.get(position);

		tv1.setText(p.getAddressLine(0));
		tv2.setText(p.getAddressLine(1));

		return convertView;

	}

}
