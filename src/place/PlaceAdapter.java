package place;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mag.R;

public class PlaceAdapter extends ArrayAdapter<Place> {

	private List<Place> placeList;
	private Context context;

	public PlaceAdapter(List<Place> placeList, Context ctx) {
		super(ctx, R.layout.row_layout, placeList);
		this.placeList = placeList;
		this.context = ctx;
	}

	public void changeList(List<Place> placeList) {
		this.placeList = placeList;
	}

	public View getView(int position, View convertView, ViewGroup parent) {

		if (convertView == null) {

			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.row_layout, parent, false);
		}
		TextView tv = (TextView) convertView.findViewById(R.id.name);
		TextView distView = (TextView) convertView.findViewById(R.id.dist);

		Place p = placeList.get(position);

		tv.setText(p.getName());

		String tmp = "";

		if (p.getPlaceTime().getHours() != 0) {

			tmp = tmp + p.getPlaceTime().getHours() + "h ";
		}
		if (p.getPlaceTime().getMinutes() != 0) {

			tmp = tmp + p.getPlaceTime().getMinutes() + "min ";
		}

		distView.setText(tmp);

		return convertView;

	}
}
