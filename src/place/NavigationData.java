package place;

import java.util.ArrayList;

import com.google.android.gms.maps.model.LatLng;

public class NavigationData {

	private ArrayList<LatLng> naviPoints = new ArrayList<LatLng>();
	private ArrayList<String> naviDistance = new ArrayList<String>();
	private ArrayList<String> naviDuration = new ArrayList<String>();
	private ArrayList<String> naviInstruction = new ArrayList<String>();
	private ArrayList<String> naviManeuvers = new ArrayList<String>();

	public ArrayList<LatLng> getNaviPoints() {
		return naviPoints;
	}

	public void setNaviPoints(ArrayList<LatLng> naviPoints) {
		this.naviPoints = naviPoints;
	}

	public ArrayList<String> getNaviDistance() {
		return naviDistance;
	}

	public void setNaviDistance(ArrayList<String> naviDistance) {
		this.naviDistance = naviDistance;
	}

	public ArrayList<String> getNaviDuration() {
		return naviDuration;
	}

	public void setNaviDuration(ArrayList<String> naviDuration) {
		this.naviDuration = naviDuration;
	}

	public ArrayList<String> getNaviInstruction() {
		return naviInstruction;
	}

	public void setNaviInstruction(ArrayList<String> naviInstrution) {
		this.naviInstruction = naviInstrution;
	}

	public ArrayList<String> getNaviManeuvers() {
		return naviManeuvers;
	}

	public void setNaviManeuvers(ArrayList<String> naviManeuvers) {
		this.naviManeuvers = naviManeuvers;
	}

}
