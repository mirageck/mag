package place;

import java.util.Date;
import java.util.concurrent.TimeUnit;
import parking.ParkingLocation;
import time.MyDate;
import time.MyTime;
import android.location.Location;
import com.google.android.gms.maps.model.Marker;

public class Place {

	private String name;

	private MyTime placeTime = new MyTime();
	private MyTime driveTime = new MyTime();
	private MyDate startActivityDate;
	private MyDate parkingDate;
	private Date startDriveDate;
	private double distanceToParking = 0;
	private Location location = new Location("point");
	private ParkingLocation parkingLocation;
	public Marker marker;

	private int parkingCorectionMinutes = 0;

	private boolean isParking = false;

	private NavigationData navigationData = new NavigationData();

	public NavigationData getNavigationData() {
		return navigationData;
	}

	public MyTime getPlaceTime() {
		return placeTime;
	}

	public void setPlaceTime(MyTime placeTime) {
		this.placeTime = placeTime;
	}

	public MyTime getDriveTime() {
		return driveTime;
	}

	public void setDriveTime(MyTime driveTime) {
		this.driveTime = driveTime;
	}

	public void setNavigationData(NavigationData navi) {
		this.navigationData = navi;
	}

	public void setParkingDate(Date date) {
		this.parkingDate = new MyDate(date, placeTime.getHours(),
				placeTime.getMinutes());
	}

	public double getDistanceToParking() {
		return distanceToParking;
	}

	public void setDistanceToParking(double odleglosc) {
		this.distanceToParking = odleglosc;
	}

	public Place(String s1, int h, int min) {

		name = s1;
		placeTime = new MyTime(h, min);

	}

	public Place(String string, int h, int min, double lat, double lng,
			Date date) {
		name = string;
		placeTime = new MyTime(h, min);

		setStartActivityDate(date);
		startDriveDate = date;
		location.setLatitude(lat);
		location.setLongitude(lng);

	}

	public double distanceTo(Location dest) {

		return location.distanceTo(dest);

	}

	public String getName() {

		return name;
	}

	public void setParkingPlace(ParkingLocation parkingLocation) {
		this.parkingLocation = parkingLocation;
	}

	public ParkingLocation getParkingPlace() {
		return parkingLocation;
	}

	public void showMarker() {

		marker.setVisible(true);
	}

	public boolean isParking() {
		return isParking;
	}

	public void setParking(boolean isParking) {
		this.isParking = isParking;
	}

	public Date getParkingDate() {

		return parkingDate.getTimeStart();
	}

	public Date getParkingDateEnd() {
		return parkingDate.getTimeEnd();
	}

	public Location getLocation() {

		return location;
	}

	public Marker getMarker() {

		return marker;
	}

	public Date getStartActivityDate() {
		return startActivityDate.getTimeStart();
	}

	public Date getStartActivityDateEnd() {
		return startActivityDate.getTimeEnd();
	}

	public void setStartActivityDate(Date date) {
		this.startActivityDate = new MyDate(date, placeTime.getHours(),
				placeTime.getMinutes());
	}

	public Date getStartDriveDate() {
		return startDriveDate;
	}

	public void setStartDriveDate(Date startDriveDate) {
		this.startDriveDate = startDriveDate;
	}

	public void remove() {

		marker.remove();

		if (isParking) {
			getParkingPlace().removeReservation(getParkingDate());

		}

	}

	public boolean reservedParking() {
		if (isParking) {
			return parkingLocation.addReservation(name, parkingDate);

		} else {
			return false;
		}
	}

	public boolean difStartTimeParkingTime() {

		setParkingCorectionMinutes(0);
		long diff = parkingDate.getTimeStart().getTime()
				- startActivityDate.getTimeStart().getTime();

		long minutes = TimeUnit.MILLISECONDS.toMinutes(diff);

		if ((int) minutes <= 60 && (int) minutes > 0) {

			setParkingCorectionMinutes(((int) minutes));
			return true;
		} else {

			return false;
		}
	}

	public int getParkingCorectionMinutes() {
		return parkingCorectionMinutes;
	}

	public void setParkingCorectionMinutes(int parkingCorectionMins) {
		this.parkingCorectionMinutes = parkingCorectionMins;
	}

	public void removeReservation() {

		isParking = false;
		distanceToParking = 0;
		parkingLocation.removeReservation(parkingDate);

	}

}
