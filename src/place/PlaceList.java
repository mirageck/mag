package place;

import java.util.ArrayList;
import java.util.List;

public class PlaceList {

	List<ArrayList<Place>> list = new ArrayList<ArrayList<Place>>();
	private boolean[] mylocation = new boolean[5];

	public PlaceList() {

		list.add(new ArrayList<Place>());
		list.add(new ArrayList<Place>());
		list.add(new ArrayList<Place>());
		list.add(new ArrayList<Place>());
		list.add(new ArrayList<Place>());
	}

	public void eliminate(int index) {

		for (int i = 0; i < list.get(index).size(); i++) {

			if (list.get(index).get(i).getName().equals("Moja Lokalizacja")) {
				list.get(index).remove(i);
				break;
			}
		}

		mylocation[index] = false;

	}

	public int size(int i) {

		return list.get(i).size();

	}

	public Place get(int i, int j) {

		return list.get(i).get(j);

	}

	public ArrayList<Place> getList(int i) {

		return list.get(i);

	}

	public void clear(int listNumber) {

		list.get(listNumber).clear();
		// TODO Auto-generated method stub

	}

	public void add(int listNumber, Place place) {
		// TODO Auto-generated method stub
		list.get(listNumber).add(place);

	}

	public void remove(int listNumber, int i) {

		list.get(listNumber).remove(i);
		// TODO Auto-generated method stub

	}

	public Place getLast(int listNumber) {
		// TODO Auto-generated method stub
		return list.get(listNumber).get(list.get(listNumber).size() - 1);
	}

	public void clearAll() {
		// TODO Auto-generated method stub
		for (int i = 0; i < list.size(); i++) {
			list.get(i).clear();
		}

	}

	public int size() {
		// TODO Auto-generated method stub
		return list.size();
	}

	public boolean isMyLocation(int index) {
		// TODO Auto-generated method stub
		return mylocation[index];
	}

	public void setMyLocation(int index, boolean value) {
		mylocation[index] = value;

	}

}
