package place;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import utility.Alert;

public class GoogleNavi {

	public void navigateToParking(Place place, Context context) {
		// TODO Auto-generated method stub

		if (place.isParking()) {

			Intent intent = new Intent(Intent.ACTION_VIEW,
					Uri.parse("google.navigation:q="
							+ place.getParkingPlace().getLatitude() + ","
							+ place.getParkingPlace().getLongitude()));
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.startActivity(intent);
		} else {
			Alert alert = new Alert();
			alert.showAlertDialog(
					context,
					"Fail",
					"Nie ma znalezionego jeszce parkingu dla tego miejsca w zadanym obr�bie",
					false);
		}

	}

	public void navigate(Place place, Context context) {
		// TODO Auto-generated method stub
		Intent intent = new Intent(Intent.ACTION_VIEW,
				Uri.parse("google.navigation:q=" + place.getLocation().getLatitude() + ","
						+ place.getLocation().getLongitude()));
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(intent);

	}

}
