package place;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class TestPlace {

	GoogleMap map;

	public TestPlace(GoogleMap map) {

		this.map = map;

	}

	public void addAll(ArrayList<Place> arrayList, int size) {

		List<Place> list = new ArrayList<Place>();

		Date date = new Date();
		Place p1 = new Place("Puscha", 1, 0, 50.8837636, 20.5943849, date);
		Place p2 = new Place("Galeria", 1, 0, 50.8807193, 20.6465967, date);
		Place p3 = new Place("Wspolna", 1, 0, 50.872477, 20.621303, date);
		Place p4 = new Place("Real", 1, 0, 50.8866758, 20.6667677, date);
		Place p5 = new Place("Tesco", 1, 0, 50.8625158, 20.6467416, date);
		Place p6 = new Place("Dewonska", 1, 0, 50.8848522, 20.585344, date);
		Place p7 = new Place("Wesola", 1, 0, 50.8691356, 20.6318602, date);
		Place p8 = new Place("Kremowa", 1, 0, 50.8167002, 20.6502843, date);
		Place p9 = new Place("Politechnika", 1, 0, 50.8798959, 20.6401886, date);
		Place p10 = new Place("Sienkiewicza", 1, 0, 50.8713857, 20.6272468,
				date);

		p1.marker = map.addMarker(new MarkerOptions().title(p1.getName())
				.position(
						new LatLng(p1.getLocation().getLatitude(), p1
								.getLocation().getLongitude())));
		p1.marker.setVisible(false);
		p2.marker = map.addMarker(new MarkerOptions().title(p2.getName())
				.position(
						new LatLng(p2.getLocation().getLatitude(), p2
								.getLocation().getLongitude())));
		p2.marker.setVisible(false);
		p3.marker = map.addMarker(new MarkerOptions().title(p3.getName())
				.position(
						new LatLng(p3.getLocation().getLatitude(), p3
								.getLocation().getLongitude())));
		p3.marker.setVisible(false);
		p4.marker = map.addMarker(new MarkerOptions().title(p4.getName())
				.position(
						new LatLng(p4.getLocation().getLatitude(), p4
								.getLocation().getLongitude())));
		p4.marker.setVisible(false);
		p5.marker = map.addMarker(new MarkerOptions().title(p5.getName())
				.position(
						new LatLng(p5.getLocation().getLatitude(), p5
								.getLocation().getLongitude())));
		p5.marker.setVisible(false);
		p6.marker = map.addMarker(new MarkerOptions().title(p6.getName())
				.position(
						new LatLng(p6.getLocation().getLatitude(), p6
								.getLocation().getLongitude())));
		p6.marker.setVisible(false);
		p7.marker = map.addMarker(new MarkerOptions().title(p7.getName())
				.position(
						new LatLng(p7.getLocation().getLatitude(), p7
								.getLocation().getLongitude())));
		p7.marker.setVisible(false);
		p8.marker = map.addMarker(new MarkerOptions().title(p8.getName())
				.position(
						new LatLng(p8.getLocation().getLatitude(), p8
								.getLocation().getLongitude())));
		p8.marker.setVisible(false);
		p9.marker = map.addMarker(new MarkerOptions().title(p9.getName())
				.position(
						new LatLng(p9.getLocation().getLatitude(), p9
								.getLocation().getLongitude())));
		p9.marker.setVisible(false);
		p10.marker = map.addMarker(new MarkerOptions().title(p10.getName())
				.position(
						new LatLng(p10.getLocation().getLatitude(), p10
								.getLocation().getLongitude())));
		p10.marker.setVisible(false);

		list.add(p1);
		list.add(p2);
		list.add(p3);
		list.add(p4);
		list.add(p5);
		list.add(p6);
		list.add(p7);
		list.add(p8);
		list.add(p9);
		list.add(p10);

		for (int i = 0; i < size; i++) {

			arrayList.add(list.get(i));

		}

		// TODO Auto-generated method stub

	}

	public void addALL(PlaceList placeList, int size) {

		for (int i = 0; i < placeList.size(); i++) {
			addAll(placeList.getList(i), size);
		}
		// TODO Auto-generated method stub

	}

	public void addONE(PlaceList placeList) {

		for (int i = 0; i < placeList.size(); i++) {

			Date date = new Date();
			Place p1 = new Place("Real", 1, 0, 50.8866758, 20.6667677, date);
			Place p2 = new Place("Tesco", 1, 0, 50.8625158, 20.6467416, date);
			Place p3 = new Place("Grunwaldzka 13", 1, 0, 50.872601, 20.610871, date);
			Place p4 = new Place("Toporowskiego", 1, 0, 50.887189, 20.639240, date);
			Place p5 = new Place("Wiosenna", 1, 0, 50.881429, 20.632562, date);
			Place p6 = new Place("Prosta 23", 1, 0, 50.862752, 20.635649, date);

			p1.marker = map.addMarker(new MarkerOptions().title(p1.getName())
					.position(
							new LatLng(p1.getLocation().getLatitude(), p1
									.getLocation().getLongitude())));
			p1.marker.setVisible(false);
			p2.marker = map.addMarker(new MarkerOptions().title(p2.getName())
					.position(
							new LatLng(p2.getLocation().getLatitude(), p2
									.getLocation().getLongitude())));
			p2.marker.setVisible(false);
			p3.marker = map.addMarker(new MarkerOptions().title(p3.getName())
					.position(
							new LatLng(p3.getLocation().getLatitude(), p3
									.getLocation().getLongitude())));
			p3.marker.setVisible(false);
			p4.marker = map.addMarker(new MarkerOptions().title(p4.getName())
					.position(
							new LatLng(p4.getLocation().getLatitude(), p4
									.getLocation().getLongitude())));
			p4.marker.setVisible(false);
			p5.marker = map.addMarker(new MarkerOptions().title(p5.getName())
					.position(
							new LatLng(p5.getLocation().getLatitude(), p5
									.getLocation().getLongitude())));
			p5.marker.setVisible(false);
			p6.marker = map.addMarker(new MarkerOptions().title(p6.getName())
					.position(
							new LatLng(p6.getLocation().getLatitude(), p6
									.getLocation().getLongitude())));
			p6.marker.setVisible(false);

			placeList.getList(i).add(p1);
			placeList.getList(i).add(p2);
			placeList.getList(i).add(p3);
			placeList.getList(i).add(p4);
			placeList.getList(i).add(p5);
			placeList.getList(i).add(p6);

		}
	}
}
