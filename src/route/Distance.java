package route;

import java.util.Calendar;
import java.util.Date;

import place.Place;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;

public class Distance {

	private Place from;
	private double distance;
	private Place to;
	private PolylineOptions route;
	private boolean calculated;
	private LatLng origin;
	private LatLng dest;

	public Distance(Place place, Place place2) {

		this.from = place;
		this.to = place2;
		this.origin = new LatLng(getFrom().getLocation().getLatitude(),
				getFrom().getLocation().getLongitude());
		this.dest = new LatLng(getTo().getLocation().getLatitude(), getTo()
				.getLocation().getLongitude());
		calculated = false;

		// TODO Auto-generated constructor stub
	}

	public Place getFrom() {
		return from;
	}

	public void setFrom(Place from) {
		this.from = from;
	}

	public Place getTo() {
		return to;
	}

	public void setTo(Place to) {
		this.to = to;
	}

	public void setRoute(PolylineOptions lineOptions) {
		// TODO Auto-generated method stub
		route = lineOptions;
	}

	public PolylineOptions getRoute() {
		// TODO Auto-generated method stub
		return route;
	}

	public boolean isCalculated() {
		// TODO Auto-generated method stub
		return calculated;
	}

	public void setCalculated() {
		// TODO Auto-generated method stub
		calculated = true;
	}

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	public int getHours() {

		return to.getDriveTime().getHours();
	}

	public void setHours(int hours) {
		to.getDriveTime().setHours(hours);

	}

	public int getMins() {

		return to.getDriveTime().getMinutes();
	}

	public void setMins(int min) {
		to.getDriveTime().setMinutes(min);

	}

	public LatLng getDest() {
		return dest;
	}

	public void setDest(LatLng dest) {
		this.dest = dest;
	}

	public LatLng getDestFromParking() {

		if (to.isParking()) {

			return new LatLng(getTo().getParkingPlace().getLatitude(), getTo()
					.getParkingPlace().getLongitude());

		} else
			return dest;

	}

	public LatLng getOrginFromParking() {

		if (from.isParking()) {

			return new LatLng(getFrom().getParkingPlace().getLatitude(),
					getFrom().getParkingPlace().getLongitude());

		} else
			return origin;

	}

	public LatLng getOrigin() {
		return origin;
	}

	public void setOrigin(LatLng origin) {
		this.origin = origin;
	}

	public void createStartTimes(boolean isParking) {
		if (from.getName().equals("Moja Lokalizacja")) {
			from.setStartActivityDate(new Date());
		}

		Date tmp1;
		Date tmp2;

		Calendar cal = Calendar.getInstance();
		cal.setTime(from.getStartActivityDateEnd());
		if (isParking) {
			if (to.getParkingCorectionMinutes() > 0) {

				int timeSum = to.getParkingCorectionMinutes();

				cal.add(Calendar.MINUTE, +timeSum);
				tmp1 = cal.getTime();

			}
		}
		tmp1 = cal.getTime();

		to.setStartDriveDate(tmp1);

		cal.setTime(tmp1);

		cal.add(Calendar.HOUR, +getHours());
		tmp2 = cal.getTime();

		cal.add(Calendar.MINUTE, +getMins());
		tmp2 = cal.getTime();

		to.setStartActivityDate(tmp2);
		// TODO Auto-generated method stub
	}

	public void createStartTimes(boolean b, Date date) {
		// TODO Auto-generated method stub
		if (from.getName().equals("Moja Lokalizacja")) {
			from.setStartActivityDate(new Date());
		}

		Date tmp1;
		Date tmp2;

		Calendar cal = Calendar.getInstance();

		if (date.after(from.getStartActivityDateEnd())) {

			cal.setTime(date);
		} else {

			cal.setTime(from.getStartActivityDateEnd());

		}
		if (b) {
			if (to.getParkingCorectionMinutes() > 0) {

				int timeSum = to.getParkingCorectionMinutes();

				cal.add(Calendar.MINUTE, +timeSum);
				tmp1 = cal.getTime();

			}
		}
		tmp1 = cal.getTime();

		to.setStartDriveDate(tmp1);

		cal.setTime(tmp1);

		cal.add(Calendar.HOUR, +getHours());
		tmp2 = cal.getTime();

		cal.add(Calendar.MINUTE, +getMins());
		tmp2 = cal.getTime();

		to.setStartActivityDate(tmp2);

	}

}
