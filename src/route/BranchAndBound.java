package route;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import android.app.Activity;

import parking.ParkingDatabase;
import place.Place;
import place.PlaceList;
import utility.Alert;

public class BranchAndBound {

	GoogleDirections googleDirection = new GoogleDirections();
	DistanceMethods distanceMethods = new DistanceMethods();

	private DistanceListMap placeDistanceMap = new DistanceListMap();

	BestList best;
	ParkingDatabase parkingDatabes;
	int maxDistanceToParking;
	PlaceList placeList;
	Activity activity;
	private boolean work = true;
	private boolean realDistances = false;

	List<ArrayList<Coordinates>> bestTmp = new ArrayList<ArrayList<Coordinates>>();
	List<ArrayList<Integer>> last = new ArrayList<ArrayList<Integer>>();

	public BranchAndBound(BestList best, ParkingDatabase parkingDatabes,
			int maxDistanceToParking, PlaceList placeList, Activity activity) {

		last.add(new ArrayList<Integer>());
		last.add(new ArrayList<Integer>());
		last.add(new ArrayList<Integer>());
		last.add(new ArrayList<Integer>());
		last.add(new ArrayList<Integer>());
		bestTmp.add(new ArrayList<Coordinates>());
		bestTmp.add(new ArrayList<Coordinates>());
		bestTmp.add(new ArrayList<Coordinates>());
		bestTmp.add(new ArrayList<Coordinates>());

		this.best = best;
		this.parkingDatabes = parkingDatabes;
		this.maxDistanceToParking = maxDistanceToParking;
		this.placeList = placeList;
		this.activity = activity;
	}

	public void calculateRouteIncludeParkings(final int index) {

		new Thread(new Runnable() {
			public void run() {

				Date start = new Date();

				distanceMethods.getDistanceBettwenPlaces(index,
						placeDistanceMap, placeList, realDistances);

				show(index);
				int size = placeList.size(index);

				cut(index, size);

				System.out.println(size);
				for (int i = 0; i < bestTmp.get(index).size(); i++) {

					System.out.println(bestTmp.get(index).get(i).getX() + " : "
							+ bestTmp.get(index).get(i).getY());
				}

				int last = 0;

				for (int i = 0; i < bestTmp.get(index).size(); i++) {
					if (bestTmp.get(index).get(i).getX() == placeList
							.size(index) - 1) {
						best.add(
								index,
								placeDistanceMap.get(index, bestTmp.get(index)
										.get(i).getX(),
										bestTmp.get(index).get(i).getY()));

						last = bestTmp.get(index).get(i).getY();
						googleDirection.calculateDistance(best.getLast(index),
								work);
						best.getLast(index).createStartTimes(false);
						if (parkingDatabes.search(best.getLast(index).getTo(),
								maxDistanceToParking)) {
							best.getLast(index).getTo()
									.difStartTimeParkingTime();
							best.getLast(index).createStartTimes(true);
						}

						best.setStartDate(index);

					}

				}

				while (best.size(index) != bestTmp.get(index).size()) {

					System.out.println(best.size(index));
					System.out.println(bestTmp.get(index).size());
					for (int i = 0; i < size; i++) {
						if (bestTmp.get(index).get(i).getX() == last) {

							last = bestTmp.get(index).get(i).getY();
							best.add(index, placeDistanceMap.get(index, bestTmp
									.get(index).get(i).getX(),
									bestTmp.get(index).get(i).getY()));
							googleDirection.calculateDistance(
									best.getLast(index), work);
							best.getLast(index).createStartTimes(false);

							if (parkingDatabes.search(best.getLast(index)
									.getTo(), maxDistanceToParking)) {
								best.getLast(index).getTo()
										.difStartTimeParkingTime();
								best.getLast(index).createStartTimes(true);
							}
							break;

						}

					}
				}
				best.setEndDate(index);

				for (int i = 0; i < best.size(index); i++) {

					System.out.println(best.get(index, i).getFrom().getName()
							+ "---" + best.get(index, i).getTo().getName());

				}

				Date end = new Date();

				long diff = end.getTime() - start.getTime();

				long seconds = TimeUnit.MILLISECONDS.toSeconds(diff);

				showToast("Trasa numer: " + index + " opracowana, czas: "
						+ seconds + "s");

				best.setTimes(seconds, index);
				placeList.eliminate(index);
			}
		}).start();

	}

	private LbValue findY(int index, double max) {

		List<Double> minsY = new ArrayList<Double>();
		int zero = 0;

		for (int y = 0; y < placeDistanceMap.getSizeY(index); y++) {

			minsY.add(9999999.0);
			zero = 0;
			for (int x = 0; x < placeDistanceMap.getSizeX(index, y); x++) {

				if (placeDistanceMap.get(index, y, x).getDistance() < minsY
						.get(minsY.size() - 1)) {
					if (placeDistanceMap.get(index, y, x).getDistance() != -1) {

						if (placeDistanceMap.get(index, y, x).getDistance() == 0) {

							zero++;
							if (zero > 2) {
								minsY.set(minsY.size() - 1, placeDistanceMap
										.get(index, y, x).getDistance());

							}
						} else {
							minsY.set(minsY.size() - 1,
									placeDistanceMap.get(index, y, x)
											.getDistance());
						}

					}
				}

			}
			if (minsY.get(minsY.size() - 1) == 9999999.0) {
				minsY.set(minsY.size() - 1, 0.0);
			}

		}

		double maxY = 0;
		int indexYY = 0;
		int indexYX = 0;
		int YYvalue = 0;

		double tmpMax = 999999;

		if (max > 0) {
			tmpMax = max;
		}

		for (int i = 0; i < minsY.size(); i++) {
			if (minsY.get(i) > maxY) {
				if (minsY.get(i) < tmpMax) {
					maxY = minsY.get(i);
					indexYY = i;
				}
			}
		}

		List<TmpInteger> listYX = new ArrayList<TmpInteger>();

		for (int x = 0; x < placeDistanceMap.getSizeX(index, indexYY); x++) {

			if (placeDistanceMap.get(index, indexYY, x).getDistance() == 0) {
				listYX.add(new TmpInteger(x, 0));

			}

		}
		for (int i = 0; i < listYX.size(); i++) {

			listYX.get(i).setValue(
					check(index, indexYY, listYX.get(i).getIndex()));
		}

		if (listYX.size() > 1) {

			int min = 9999;
			int tmpIndex = 0;

			for (int i = 0; i < listYX.size(); i++) {
				if (listYX.get(i).getValue() <= min) {
					if (listYX.get(i).getValue() != 0) {
						if (checkBack(index, listYX.get(i).getIndex())) {
							min = listYX.get(i).getValue();
							tmpIndex = i;
						}
					}
				}

			}
			indexYX = listYX.get(tmpIndex).getIndex();
			YYvalue = listYX.get(tmpIndex).getValue();

		} else {

			indexYX = listYX.get(0).getIndex();
			YYvalue = listYX.get(0).getValue();
		}

		LbValue ret = new LbValue(indexYX, indexYY, YYvalue, maxY);
		return ret;
	}

	private LbValue findX(int index, double max) {

		int zero = 0;

		List<Double> minsX = new ArrayList<Double>();

		for (int x = 0; x < placeDistanceMap.getSizeX(index); x++) {

			minsX.add(9999999.0);
			zero = 0;
			for (int y = 0; y < placeDistanceMap.getSizeY(index); y++) {

				if (placeDistanceMap.get(index, y, x).getDistance() < minsX
						.get(minsX.size() - 1)) {
					if (placeDistanceMap.get(index, y, x).getDistance() != -1) {

						if (placeDistanceMap.get(index, y, x).getDistance() == 0) {

							zero++;
							if (zero > 2) {
								minsX.set(minsX.size() - 1, placeDistanceMap
										.get(index, y, x).getDistance());

							}
						} else {
							minsX.set(minsX.size() - 1,
									placeDistanceMap.get(index, y, x)
											.getDistance());
						}
					}
				}

			}
			if (minsX.get(minsX.size() - 1) == 9999999.0) {
				minsX.set(minsX.size() - 1, 0.0);
			}

		}

		show(index);

		double maxX = 0.0;
		int indexXX = 0;
		int indexXY = 0;
		int XXvalue = 0;

		double tmpMax = 999999;

		if (max > 0) {
			tmpMax = max;
		}

		for (int i = 0; i < minsX.size(); i++) {
			if (minsX.get(i) > maxX) {

				if (minsX.get(i) < tmpMax) {
					maxX = minsX.get(i);
					indexXX = i;
				}

			}
		}
		List<TmpInteger> listXY = new ArrayList<TmpInteger>();

		for (int y = 0; y < placeDistanceMap.getSizeX(index); y++) {

			if (placeDistanceMap.get(index, y, indexXX).getDistance() == 0) {
				listXY.add(new TmpInteger(y, 0));
				// indexYX = x;

			}

		}
		for (int i = 0; i < listXY.size(); i++) {

			listXY.get(i).setValue(
					check(index, listXY.get(i).getIndex(), indexXY));
			System.out.println("XX:" + indexXX + "  Y:"
					+ listXY.get(i).getIndex());
		}

		if (listXY.size() > 1) {

			int min = 9999;
			int tmpIndex = 0;

			for (int i = 0; i < listXY.size(); i++) {
				if (listXY.get(i).getValue() <= min) {
					if (listXY.get(i).getValue() != 0) {
						if (checkBack(index, listXY.get(i).getIndex())) {
							min = listXY.get(i).getValue();
							tmpIndex = i;
						}
					}
				}

			}
			indexXY = listXY.get(tmpIndex).getIndex();
			// last.get(index).add(listYX.get(tmpIndex).getValue());
			XXvalue = listXY.get(tmpIndex).getValue();

		} else {

			indexXY = listXY.get(0).getIndex();
			// last.get(index).add(listYX.get(0).getValue());
			XXvalue = listXY.get(0).getValue();
		}

		LbValue ret = new LbValue(indexXX, indexXY, XXvalue, maxX);
		return ret;
	}

	private void cut(int index, int size) {

		if (bestTmp.get(index).size() == size - 2) {

			System.out.println("FINITO");

			show(index);

			for (int y = 0; y < placeDistanceMap.getSizeY(index); y++) {

				boolean end = false;
				for (int x = 0; x < placeDistanceMap.getSizeX(index, y); x++) {
					if (placeDistanceMap.get(index, y, x).getDistance() > 0) {
						bestTmp.get(index).add(new Coordinates(x, y));
						putNegatives(index, y, x, false);
						placeDistanceMap.get(index, x, y).setDistance(-1);
						show(index);
						end = true;
						break;
					}
				}
				if (!end) {
					int tmpx = -1;
					for (int x = 0; x < placeDistanceMap.getSizeX(index, y); x++) {
						if (placeDistanceMap.get(index, y, x).getDistance() != -1) {

							// break;
							tmpx = x;
						}
					}
					if (tmpx > -1) {

						show(index);
						end = true;
						bestTmp.get(index).add(new Coordinates(tmpx, y));
						placeDistanceMap.get(index, tmpx, y).setDistance(-1);
						putNegatives(index, y, tmpx, true);
					}
				}

			}
		} else {

			shorter(index);

			int indexY = 0;
			int indexX = 0;

			LbValue findX = findX(index, 0);
			LbValue findY = findY(index, 0);

			boolean done = false;
			System.out.println("TMP{");
			System.out.println("index XX:" + findX.getX() + " -- index Y:"
					+ findX.getY() + ": " + findX.getValue() + " max:"
					+ findX.getMax());
			System.out.println("index X:" + findY.getX() + " -- index YY:"
					+ findY.getY() + ": " + findY.getValue() + " max:"
					+ findY.getMax());

			System.out.println("}TMP");
			if (!checkBack2(index, findY.getY())
					&& !checkBack2(index, findX.getY())) {

				findX = findX(index, findX.getMax());
				findY = findY(index, findY.getMax());

				System.out.println("Ponowne TMP{");
				System.out.println("index XX:" + findX.getX() + " -- index Y:"
						+ findX.getY() + ": " + findX.getValue() + "max "
						+ findX.getMax());
				System.out.println("index X:" + findY.getX() + " -- index YY:"
						+ findY.getY() + ": " + findY.getValue() + "max "
						+ findY.getMax());
				System.out.println("}Ponowne TMP");

			}

			if (findY.getMax() == findX.getMax()) {

				if (findY.getValue() < findX.getValue()) {

					indexX = findY.getX();
					indexY = findY.getY();
					System.out.println("Warto�c" + findY.getValue());
					last.get(index).add(findY.getValue());
				} else {

					indexX = findX.getX();
					indexY = findX.getY();
					System.out.println("Warto�c" + findX.getValue());
					last.get(index).add(findX.getValue());

				}

				done = true;
			}

			if (findY.getValue() == 0) {

				if (checkBack2(index, findX.getY())) {
					indexX = findX.getX();
					indexY = findX.getY();
					System.out.println("Warto�c" + findX.getValue());
					last.get(index).add(findX.getValue());
					done = true;
				}
			}

			if (findX.getValue() == 0) {

				if (checkBack2(index, findY.getY())) {
					indexX = findY.getX();
					indexY = findY.getY();
					System.out.println("Warto�c" + findY.getValue());
					last.get(index).add(findY.getValue());
					done = true;
				}
			}

			if (!done) {
				if (findY.getMax() > findX.getMax()) {

					indexX = findY.getX();
					indexY = findY.getY();
					System.out.println("Warto�c" + findY.getValue());
					last.get(index).add(findY.getValue());
				} else {

					indexX = findX.getX();
					indexY = findX.getY();
					System.out.println("Warto�c" + findX.getValue());
					last.get(index).add(findX.getValue());

				}
			}
			System.out.println("index X:" + indexX + " -- index Y:" + indexY);

			placeDistanceMap.get(index, indexX, indexY).setDistance(-1);

			// show(index);

			putNegatives(index, indexY, indexX, false);

			bestTmp.get(index).add(new Coordinates(indexX, indexY));
			// show(index);

			cut(index, size);
		}
	}

	public void putNegatives(int index, int indexY, int indexX, boolean onlyRows) {

		if (!onlyRows) {
			for (int x = 0; x < placeDistanceMap.getSizeX(index, indexY); x++) {

				placeDistanceMap.set(index, indexY, x, -1);

			}
		}

		for (int y = 0; y < placeDistanceMap.getSizeY(index); y++) {

			placeDistanceMap.set(index, y, indexX, -1);

		}

	}

	private boolean checkBack2(int index, int past) {

		for (int i = 0; i < bestTmp.get(index).size(); i++) {
			if (bestTmp.get(index).get(i).getX() == past) {
				return false;
			}
		}

		return true;
	}

	private boolean checkBack(int index, int past) {

		for (int i = 0; i < bestTmp.get(index).size(); i++) {
			if (bestTmp.get(index).get(i).getY() == past) {
				return false;
			}
		}

		return true;
	}

	private void shorter(int index) {
		double min = 999999;
		double minX = 0;
		for (int y = 0; y < placeDistanceMap.getSizeY(index); y++) {

			min = 999999;
			for (int x = 0; x < placeDistanceMap.getSizeX(index, y); x++) {

				if (placeDistanceMap.get(index, y, x).getDistance() < min) {
					if (placeDistanceMap.get(index, y, x).getDistance() != -1) {

						min = placeDistanceMap.get(index, y, x).getDistance();
					}
				}

			}

			if (min != 999999) {
				for (int x = 0; x < placeDistanceMap.getSizeX(index, y); x++) {

					if (placeDistanceMap.get(index, y, x).getDistance() != -1) {
						placeDistanceMap.get(index, y, x).setDistance(
								placeDistanceMap.get(index, y, x).getDistance()
										- min);
					}
				}

				minX = minX + min;
			}
		}

		double min2 = 999999;
		double minY = 0;
		for (int x = 0; x < placeDistanceMap.getSizeX(index); x++) {

			min2 = 999999;
			for (int y = 0; y < placeDistanceMap.getSizeY(index); y++) {

				if (placeDistanceMap.get(index, y, x).getDistance() < min2) {
					if (placeDistanceMap.get(index, y, x).getDistance() != -1) {
						min2 = placeDistanceMap.get(index, y, x).getDistance();
					}
				}

			}
			if (min2 != 999999) {
				for (int y = 0; y < placeDistanceMap.getSizeY(index); y++) {

					if (placeDistanceMap.get(index, y, x).getDistance() != -1) {
						placeDistanceMap.get(index, y, x).setDistance(
								placeDistanceMap.get(index, y, x).getDistance()
										- min2);
					}

				}

				minY = minY + min2;
			}

		}
		double sum = minX + minY;

	}

	private int check(int index, int indexX, int indexY) {
		double min = 999999;
		double minX = 0;
		for (int y = 0; y < placeDistanceMap.getSizeY(index); y++) {

			min = 999999;
			for (int x = 0; x < placeDistanceMap.getSizeX(index, y); x++) {

				if (placeDistanceMap.get(index, y, x).getDistance() < min) {

					if (x != indexY && y != indexX) {
						if (x != indexX || y != indexY) {
							if (placeDistanceMap.get(index, y, x).getDistance() != -1) {

								min = placeDistanceMap.get(index, y, x)
										.getDistance();
							}
						}
					}
				}

			}

			if (min != 999999) {

				minX = minX + min;
			}
		}

		double min2 = 999999;
		double minY = 0;
		for (int x = 0; x < placeDistanceMap.getSizeX(index); x++) {

			min2 = 999999;
			for (int y = 0; y < placeDistanceMap.getSizeY(index); y++) {

				if (placeDistanceMap.get(index, y, x).getDistance() < min2) {
					if (x != indexY && y != indexX) {

						if (x != indexX || y != indexY) {

							if (placeDistanceMap.get(index, y, x).getDistance() != -1) {
								min2 = placeDistanceMap.get(index, y, x)
										.getDistance();
							}
						}
					}
				}

			}

			if (min2 != 999999) {

				minY = minY + min2;

			}

		}
		double sum = minX + minY;

		return (int) sum;

	}

	public void calculateRoute(final int index) {

		new Thread(new Runnable() {
			public void run() {

				Date start = new Date();

				distanceMethods.getDistanceBettwenPlaces(index,
						placeDistanceMap, placeList, realDistances);

				show(index);
				int size = placeList.size(index);

				cut(index, size);

				List<Place> placeListTmp = new ArrayList<Place>();

				int last = 0;

				for (int i = 0; i < bestTmp.get(index).size(); i++) {
					if (bestTmp.get(index).get(i).getX() == placeList
							.size(index) - 1) {

						placeListTmp.add(placeDistanceMap.get(index,
								bestTmp.get(index).get(i).getX(),
								bestTmp.get(index).get(i).getY()).getFrom());

						last = bestTmp.get(index).get(i).getY();

					}

				}

				while (best.size(index) != bestTmp.get(index).size()) {

					for (int i = 0; i < size; i++) {
						if (bestTmp.get(index).get(i).getX() == last) {

							last = bestTmp.get(index).get(i).getY();

							placeListTmp
									.add(placeDistanceMap.get(index,
											bestTmp.get(index).get(i).getX(),
											bestTmp.get(index).get(i).getY())
											.getFrom());

							break;

						}

					}
				}

				DistanceHashMap distanceMap = new DistanceHashMap();
				distanceMethods.getDistanceBettwenPlacesHashMap(distanceMap,
						placeListTmp, realDistances);

				for (int i = 0; i < placeListTmp.size() - 1; i++) {

					Distance tmp;

					tmp = distanceMap.get(placeListTmp.get(i).getName(),
							placeListTmp.get(i + 1).getName());
					googleDirection.calculateDistance(tmp, work);
					tmp.createStartTimes(false);

					if (parkingDatabes.search(tmp.getTo(), maxDistanceToParking)) {
						tmp.getTo().difStartTimeParkingTime();
						tmp.createStartTimes(true);
						best.add(index, tmp);

						if (i == 0) {
							best.setStartDate(index);
						}

					} else {

						if (i == 0) {

							best.add(index, tmp);
							best.setStartDate(index);

						} else {
							placeListTmp.remove(i + 1);
						}

					}

				}

				if (best.size(index) > 0) {
					Distance tmp;

					tmp = distanceMap.get(
							best.getLast(index).getTo().getName(),
							best.get(index, 0).getFrom().getName());
					googleDirection.calculateDistance(tmp, work);
					tmp.createStartTimes(false);
					if (parkingDatabes.search(tmp.getTo(), maxDistanceToParking)) {
						tmp.getTo().difStartTimeParkingTime();
						tmp.createStartTimes(true);
					}
					best.add(index, tmp);

				}

				best.setEndDate(index);

				for (int i = 0; i < best.size(index); i++) {

					System.out.println(best.get(index, i).getFrom().getName()
							+ "---" + best.get(index, i).getTo().getName());

				}

				Date end = new Date();

				long diff = end.getTime() - start.getTime();

				long seconds = TimeUnit.MILLISECONDS.toSeconds(diff);

				showToast("Trasa numer: " + index + " opracowana, czas: "
						+ seconds + "s");

				best.setTimes(seconds, index);
				placeList.eliminate(index);
			}
		}).start();

	}

	public void show(int index) {

		for (int y = 0; y < placeDistanceMap.getSizeY(index); y++) {

			if (y == 0) {
				System.out.print("       ");
				for (int yy = 0; yy < placeList.size(index); yy++) {
					System.out.print(placeList.get(index, yy).getName() + " ");

				}
				System.out.println("\n");

			}
			for (int x = 0; x < placeDistanceMap.getSizeX(index, y); x++) {

				if (x == 0) {
					System.out.print(placeList.get(index, y).getName() + " ");
				}
				System.out.print((int) placeDistanceMap.get(0, y, x)
						.getDistance() + " ");

			}
			System.out.println("\n");
		}

	}

	public void showToast(final String toast) {
		activity.runOnUiThread(new Runnable() {
			public void run() {

				Alert alert = new Alert();

				alert.showFinishDialog(activity, "Informacja", toast, false);

				// Toast.makeText(activity, toast, Toast.LENGTH_SHORT).show();
			}
		});
	}

}
