package route;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BestList {

	private List<ArrayList<Distance>> list = new ArrayList<ArrayList<Distance>>();
	private List<Long> times = new ArrayList<Long>();
	private List<Date> startDate = new ArrayList<Date>();;
	private List<Date> endDate = new ArrayList<Date>();;;

	public Date getStartDate(int i) {
		return startDate.get(i);
	}

	public Date getEndDate(int i) {
		return endDate.get(i);

	}

	public BestList() {

		list.add(new ArrayList<Distance>());
		list.add(new ArrayList<Distance>());
		list.add(new ArrayList<Distance>());
		list.add(new ArrayList<Distance>());
		list.add(new ArrayList<Distance>());
		times.add(null);
		times.add(null);
		times.add(null);
		times.add(null);
		times.add(null);
		startDate.add(new Date());
		endDate.add(new Date());
		startDate.add(new Date());
		endDate.add(new Date());
		startDate.add(new Date());
		endDate.add(new Date());
		startDate.add(new Date());
		endDate.add(new Date());
		startDate.add(new Date());
		endDate.add(new Date());

	}

	public int size(int i) {

		return list.get(i).size();

	}

	public Distance get(int i, int j) {

		return list.get(i).get(j);

	}

	public void clear(int listNumber) {

		list.get(listNumber).clear();
		// TODO Auto-generated method stub

	}

	public void add(int listNumber, Distance distance) {
		// TODO Auto-generated method stub
		list.get(listNumber).add(distance);

	}

	public void remove(int listNumber, int i) {

		if (list.get(listNumber).get(i).getTo().isParking()) {
			list.get(listNumber).get(i).getTo().removeReservation();
		}

		list.get(listNumber).remove(i);
		// TODO Auto-generated method stub

	}

	public Distance getLast(int listNumber) {
		// TODO Auto-generated method stub
		return list.get(listNumber).get(list.get(listNumber).size() - 1);
	}

	public ArrayList<Distance> getList(int index) {
		// TODO Auto-generated method stub
		return list.get(index);
	}

	public Long getTimes(int index) {
		return times.get(index);
	}

	public void setTimes(long seconds, int index) {

		times.set(index, seconds);
	}

	public List<Long> getTimes() {
		return times;
	}

	public void setDates(int index) {
		// TODO Auto-generated method stub

	}

	public void setStartDate(int index) {
		// TODO Auto-generated method stub
		startDate.set(index, get(index, 0).getFrom().getStartActivityDate());

	}

	public void setEndDate(int index) {
		// TODO Auto-generated method stub
		endDate.set(index, getLast(index).getTo().getStartActivityDateEnd());

	}

}
