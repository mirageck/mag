package route;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import place.Place;
import place.PlaceList;

public class DistanceMethods {

	GoogleDirections googleDirection = new GoogleDirections();

	public void getDistanceBettwenPlaces(int index,
			DistanceListMap placeDistanceList, PlaceList placeList,
			boolean realDistance) {

		placeDistanceList.clear(index);

		for (int i = 0; i < placeList.size(index); i++) {

			ArrayList<Distance> single_list = new ArrayList<Distance>();
			for (int j = 0; j < placeList.size(index); j++) {

				if (j == i) {

					Distance distance_tmp = new Distance(
							placeList.get(index, i), placeList.get(index, j));
					distance_tmp.setDistance(-1);
					single_list.add(distance_tmp);

				} else {
					Distance distance_tmp = new Distance(
							placeList.get(index, i), placeList.get(index, j));

					if (realDistance) {
						googleDirection.calculateDistance(distance_tmp, true);
					} else {

						distance_tmp.setDistance(placeList.get(index, i)
								.distanceTo(
										placeList.get(index, j).getLocation()));
					}
					single_list.add(distance_tmp);

				}
			}

			placeDistanceList.add(index, single_list);

		}

	}

	public void getDistanceBettwenPlaces(int index,
			DistanceMap placeDistanceMap, PlaceList placeList,
			boolean realDistance) {

		placeDistanceMap.clear(index);

		for (int i = 0; i < placeList.size(index); i++) {

			ArrayList<Distance> single_list = new ArrayList<Distance>();
			for (int j = 0; j < placeList.size(index); j++) {
				if (j == i) {
				} else {
					Distance distance_tmp = new Distance(
							placeList.get(index, i), placeList.get(index, j));

					if (realDistance) {
						googleDirection.calculateDistance(distance_tmp, true);
					} else {
						distance_tmp.setDistance(placeList.get(index, i)
								.distanceTo(
										placeList.get(index, j).getLocation()));
						single_list.add(distance_tmp);
					}
				}
			}
			sortList(single_list);
			placeDistanceMap.put(index, placeList.get(index, i).getName(),
					single_list);

		}

	}

	private void sortList(ArrayList<Distance> single_list) {
		Collections.sort(single_list, new Comparator<Distance>() {

			@Override
			public int compare(Distance arg0, Distance arg1) {
				// TODO Auto-generated method stub

				return Double.compare(arg0.getDistance(), arg1.getDistance());
			}
		});
	}

	public void getDistanceBettwenPlacesHashMap(DistanceHashMap distanceMap,
			List<Place> placeList, boolean realDistances) {

		for (int i = 0; i < placeList.size(); i++) {

			HashMap<String, Distance> single_list = new HashMap<String, Distance>();
			for (int j = 0; j < placeList.size(); j++) {
				if (j == i) {
				} else {
					Distance distance_tmp = new Distance(placeList.get(i),
							placeList.get(j));

					single_list.put(placeList.get(j).getName(), distance_tmp);
				}
			}
			distanceMap.put(placeList.get(i).getName(), single_list);
		}

	}

}
