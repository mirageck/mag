package route;

import java.util.concurrent.ExecutionException;

import utility.GettingUrlTask;

import com.google.android.gms.maps.model.LatLng;

public class GoogleDirections {

	public boolean calculateDistance(Distance dst, boolean work) {

		if (work) {

			String url = getDirectionsUrl(dst.getOrigin(), dst.getDest());

			GettingUrlTask downloadTask = new GettingUrlTask();
			downloadTask.execute(url);
			ParserTask parserTask = new ParserTask(dst);

			boolean tmp = false;
			try {
				tmp = parserTask.doInBackground(downloadTask.get());
			} catch (InterruptedException e1) {

				e1.printStackTrace();
			} catch (ExecutionException e1) {

				e1.printStackTrace();
			}
			if (!tmp) {
				calculateDistance(dst, true);
			}

			return tmp;
		} else {
			return false;
		}
	}

	private String getDirectionsUrl(LatLng origin, LatLng dest) {

		String strOrigin = "origin=" + origin.latitude + "," + origin.longitude;

		String strDest = "destination=" + dest.latitude + "," + dest.longitude;

		String sensor = "sensor=false";

		String parameters = strOrigin + "&" + strDest + "&" + sensor
				+ "&language=pl";

		String url = "https://maps.googleapis.com/maps/api/directions/json"
				+ "?" + parameters;
		// + "&key=AIzaSyC2l3iIS1fpeoRxnnF1bNcX1RyJTdgh8nY";

		return url;
	}
}
