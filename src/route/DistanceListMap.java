package route;

import java.util.ArrayList;
import java.util.List;

public class DistanceListMap {

	public List<ArrayList<ArrayList<Distance>>> map = new ArrayList<ArrayList<ArrayList<Distance>>>();

	public DistanceListMap() {

		map.add(new ArrayList<ArrayList<Distance>>());
		map.add(new ArrayList<ArrayList<Distance>>());
		map.add(new ArrayList<ArrayList<Distance>>());
		map.add(new ArrayList<ArrayList<Distance>>());
		map.add(new ArrayList<ArrayList<Distance>>());
	}

	public void add(int index, ArrayList<Distance> single_list) {

		map.get(index).add(single_list);
		// TODO Auto-generated method stub

	}

	public void clear(int index) {
		map.get(index).clear();
		// TODO Auto-generated method stub

	}

	public Distance get(int index, int indexY, int indexX) {

		return map.get(index).get(indexY).get(indexX);
	}

	public void set(int index, int indexY, int indexX, double distance) {

		map.get(index).get(indexY).get(indexX).setDistance(distance);

	}

	public int getSizeY(int index) {
		// TODO Auto-generated method stub

		return map.get(index).size();
	}

	public int getSizeX(int index, int y) {
		// TODO Auto-generated method stub
		return map.get(index).get(y).size();
	}

	public int getSizeX(int index) {
		
		
		return map.get(index).get(0).size();
	}



}
