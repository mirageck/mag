package route;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DistanceMap {

	public List<HashMap<String, ArrayList<Distance>>> map = new ArrayList<HashMap<String, ArrayList<Distance>>>();

	public DistanceMap() {

		map.add(new HashMap<String, ArrayList<Distance>>());
		map.add(new HashMap<String, ArrayList<Distance>>());
		map.add(new HashMap<String, ArrayList<Distance>>());
		map.add(new HashMap<String, ArrayList<Distance>>());
		map.add(new HashMap<String, ArrayList<Distance>>());
	}

	public ArrayList<Distance> get(int listNumber, String string) {
		// TODO Auto-generated method stub

		return map.get(listNumber).get(string);

	}

	public Distance get(int listNumber, String string, int i) {
		// TODO Auto-generated method stub

		return map.get(listNumber).get(string).get(i);

	}

	public int size2(int listNumber, String string) {
		// TODO Auto-generated method stub
		return map.get(listNumber).get(string).size();
	}
	
	public int size(int listNumber) {
		// TODO Auto-generated method stub
		return map.get(listNumber).size();
	}

	public void clear(int listNumber) {
		// TODO Auto-generated method stub
		map.get(listNumber).clear();

	}

	public void put(int listNumber, String name, ArrayList<Distance> single_list) {
		// TODO Auto-generated method stub
		map.get(listNumber).put(name, single_list);
	}

	public Distance get(int listNumber, String key1, String key2) {

		for (int i = 0; i < map.get(listNumber).get(key1).size(); i++) {

			if (key2.equals(map.get(listNumber).get(key1).get(i))) {

				return map.get(listNumber).get(key1).get(i);
			}

		}
		return null;

	}

}
