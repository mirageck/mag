package route;

public class TmpInteger {

	private int x=0;
	private int value=0;
	
	public TmpInteger(int x, int value) {
		this.setX(x);
		this.setValue(value);
		// TODO Auto-generated constructor stub
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = (int)value;
	}

	public int getIndex() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

}
