package route;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.json.JSONObject;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;
import android.graphics.Color;

public class ParserTask {

	public Distance dst;

	public ParserTask(Distance dst) {

		this.dst = dst;
	}

	public boolean doInBackground(String jsonData) {

		JSONObject jObject;
		JSONResult routes = null;
		try {
			jObject = new JSONObject(jsonData);
			JSONParser parser = new JSONParser();
			routes = parser.parse(jObject);
		} catch (Exception e) {
			e.printStackTrace();
		}
		ArrayList<LatLng> points = null;
		ArrayList<LatLng> naviPoints = null;
		ArrayList<String> naviDistance = null;
		ArrayList<String> naviDuration = null;
		ArrayList<String> naviInstruction = null;
		ArrayList<String> naviManeuvers = null;
		PolylineOptions lineOptions = null;
		Double distance = 0.0;
		Double duration_h = 0.0;
		Double duration_min = 0.0;

		if (routes.getRoutes().size() < 1) {
			System.out.println("No Points" + dst.getFrom().getName() + ":"
					+ dst.getTo().getName());
			return false;
		}

		for (int i = 0; i < routes.getRoutes().size(); i++) {

			naviPoints = new ArrayList<LatLng>();
			naviDistance = new ArrayList<String>();
			naviDuration = new ArrayList<String>();
			naviInstruction = new ArrayList<String>();
			naviManeuvers = new ArrayList<String>();
			points = new ArrayList<LatLng>();
			lineOptions = new PolylineOptions();

			List<Double> listLat = new ArrayList<Double>();
			List<Double> listLng = new ArrayList<Double>();

			List<HashMap<String, Double>> path = routes.getRoutes().get(i);
			List<HashMap<String, String>> path2 = routes.getRoutesString().get(
					i);
			List<HashMap<String, Double>> path3 = routes.getRoutesPoints().get(
					i);

			for (int j = 0; j < path.size(); j++) {

				HashMap<String, Double> point = path.get(j);

				if (j == 0) {
					distance = point.get("distance");
					continue;
				} else if (j == 1) {
					duration_h = point.get("duration_h");
					continue;
				} else if (j == 2) {
					duration_min = point.get("duration_min");
					continue;
				}

				double lat = point.get("lat");
				double lng = point.get("lng");
				LatLng position = new LatLng(lat, lng);
				points.add(position);
			}

			for (HashMap<String, Double> tmp : path3) {

				double nLat;
				double nLng;
				try {
					nLat = tmp.get("nLat");
					listLat.add(nLat);

				} catch (NullPointerException e1) {
					e1.printStackTrace();
				}

				try {
					nLng = tmp.get("nLng");
					listLng.add(nLng);
				} catch (NullPointerException e1) {
					e1.printStackTrace();
				}

			}

			for (int j = 0; j < listLat.size(); j++) {
				LatLng nPosition = new LatLng(listLat.get(j), listLng.get(j));
				naviPoints.add(nPosition);
			}

			for (HashMap<String, String> tmp : path2) {

				String tmpDistance = tmp.get("nDistance");
				String tmpDuration = tmp.get("nDuration");
				String tmpInstr = tmp.get("nInstruction");
				String tmpMan = tmp.get("nManeuver");

				try {
					if (tmpDistance.length() > 1) {
						naviDistance.add(tmpDistance);
					}
				} catch (NullPointerException e1) {

					e1.printStackTrace();
				}
				try {
					if (tmpDuration.length() > 1) {
						naviDuration.add(tmpDuration);
					}

				} catch (NullPointerException e1) {
					e1.printStackTrace();
				}

				try {
					if (tmpInstr.length() > 1) {
						naviInstruction.add(removeHtmlTags(tmpInstr));
						// naviInstruction.add(tmpInstr);
					}

				} catch (NullPointerException e1) {
					e1.printStackTrace();
				}

				try {
					if (tmpMan.length() > 1) {
						naviManeuvers.add(tmpMan);
					}

				} catch (NullPointerException e1) {
					e1.printStackTrace();
				}

			}

			lineOptions.addAll(points);
			lineOptions.width(3);
			lineOptions.color(Color.RED);
		}

		dst.setDistance(distance);
		dst.setHours(duration_h.intValue());
		dst.setMins(duration_min.intValue());
		dst.setRoute(lineOptions);
		dst.getTo().getNavigationData().setNaviDistance(naviDistance);
		dst.getTo().getNavigationData().setNaviDuration(naviDuration);
		dst.getTo().getNavigationData().setNaviInstruction(naviInstruction);
		dst.getTo().getNavigationData().setNaviManeuvers(naviManeuvers);
		dst.getTo().getNavigationData().setNaviPoints(naviPoints);
		dst.setCalculated();
		return true;
	}

	public String removeHtmlTags(String source) {

		String tmp = source.replace("<b>", "");
		tmp = tmp.replace("</b>", "");
		int index1 = tmp.indexOf('<');
		int index2 = tmp.indexOf(">");
		if (index1 > -1) {
			String tmp1 = tmp.substring(0, index1 + 1);
			String tmp2 = tmp.substring(index2 + 1, tmp.length());
			tmp1 = tmp1.replace('<', ' ');
			System.out.println("ep1:" + tmp1);
			System.out.println("ep2:" + tmp2);

			tmp = tmp1 + tmp2;
			tmp = tmp.replace("</div>", "");

		}

		do {

			index1 = tmp.indexOf('<');
			index2 = tmp.indexOf(">");
			if (index1 > -1) {

				String tmp1 = tmp.substring(0, index1 +1);
				String tmp2 = tmp.substring(index2 + 1, tmp.length());
				tmp1 = tmp1.replace('<', ' ');
				tmp = tmp1 + tmp2;

			}
		} while (index1 > -1);

		return tmp;

	}
}
