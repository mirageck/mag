package route;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.maps.model.LatLng;

public class JSONParser {
	


	public JSONResult parse(JSONObject jObject) {
		List<List<HashMap<String, Double>>> routes = new ArrayList<List<HashMap<String, Double>>>();
		List<List<HashMap<String, String>>> routesString = new ArrayList<List<HashMap<String, String>>>();
		List<List<HashMap<String, Double>>> routesPoints = new ArrayList<List<HashMap<String, Double>>>();
		JSONResult result = new JSONResult();
		JSONArray jRoutes = null;
		JSONArray jLegs = null;
		JSONArray jSteps = null;
		JSONObject jDistance = null;
		JSONObject jDuration = null;
		try {
			jRoutes = jObject.getJSONArray("routes");
			for (int i = 0; i < jRoutes.length(); i++) {

				jLegs = ((JSONObject) jRoutes.get(i)).getJSONArray("legs");
				List<HashMap<String, Double>> path = new ArrayList<HashMap<String, Double>>();
				List<HashMap<String, String>> pathString = new ArrayList<HashMap<String, String>>();
				List<HashMap<String, Double>> pathPoints = new ArrayList<HashMap<String, Double>>();
				for (int j = 0; j < jLegs.length(); j++) {

					jDistance = ((JSONObject) jLegs.get(j))
							.getJSONObject("distance");
					HashMap<String, Double> hmDistance = new HashMap<String, Double>();
					String tmp = jDistance.getString("text").substring(0,
							jDistance.getString("text").length() - 2);
					tmp = tmp.trim();
					tmp = tmp.replace(',', '.');
					hmDistance.put("distance", Double.parseDouble(tmp));
					jDuration = ((JSONObject) jLegs.get(j))
							.getJSONObject("duration");
					HashMap<String, Double> hmDuration_h = new HashMap<String, Double>();
					hmDuration_h.put(
							"duration_h",
							setDurationFromString(jDuration.getString("text"),
									0));
					HashMap<String, Double> hmDuration_min = new HashMap<String, Double>();
					hmDuration_min.put(
							"duration_min",
							setDurationFromString(jDuration.getString("text"),
									1));
					path.add(hmDistance);
					path.add(hmDuration_h);
					path.add(hmDuration_min);
					jSteps = ((JSONObject) jLegs.get(j)).getJSONArray("steps");
					for (int x = 0; x < jSteps.length(); x++) {
						String polyline = "";
						String distance = "";
						String duration = "";
						double lat;
						double lng;
						String instructions = "";
						String maneuver = "";
						distance = (String) ((JSONObject) ((JSONObject) jSteps
								.get(x)).get("distance")).get("text");
						duration = (String) ((JSONObject) ((JSONObject) jSteps
								.get(x)).get("duration")).get("text");
						HashMap<String, String> naviDistance = new HashMap<String, String>();
						naviDistance.put("nDistance", distance);
						HashMap<String, String> naviDuration = new HashMap<String, String>();
						naviDuration.put("nDuration", duration);
						lat = (Double) ((JSONObject) ((JSONObject) jSteps
								.get(x)).get("start_location")).get("lat");

						lng = (Double) ((JSONObject) ((JSONObject) jSteps
								.get(x)).get("start_location")).get("lng");
						HashMap<String, Double> naviLat = new HashMap<String, Double>();
						naviLat.put("nLat", lat);
						HashMap<String, Double> naviLng = new HashMap<String, Double>();
						naviLng.put("nLng", lng);
						pathPoints.add(naviLat);
						pathPoints.add(naviLng);
						instructions = (String) ((JSONObject) jSteps.get(x))
								.get("html_instructions");
						maneuver = " ";
						try {
							maneuver = (String) ((JSONObject) jSteps.get(x))
									.get("maneuver");
						} catch (JSONException e1) {
							maneuver = "straight";
							e1.printStackTrace();
						}
						HashMap<String, String> naviInstruction = new HashMap<String, String>();
						naviInstruction.put("nInstruction", instructions);
						HashMap<String, String> naviManeuver = new HashMap<String, String>();
						naviManeuver.put("nManeuver", maneuver);
						pathString.add(naviDistance);
						pathString.add(naviDuration);
						pathString.add(naviInstruction);
						pathString.add(naviManeuver);
						polyline = (String) ((JSONObject) ((JSONObject) jSteps
								.get(x)).get("polyline")).get("points");
						List<LatLng> list = decodePoly(polyline);
						for (int y = 0; y < list.size(); y++) {
							HashMap<String, Double> hm = new HashMap<String, Double>();
							hm.put("lat", ((LatLng) list.get(y)).latitude);
							hm.put("lng", ((LatLng) list.get(y)).longitude);
							path.add(hm);
						}
					}
				}
				routesPoints.add(pathPoints);
				routesString.add(pathString);
				routes.add(path);
				result.setAllRoutes(routes, routesString, routesPoints);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (Exception e) {
		}
		return result;
	}

	public double setDurationFromString(String duration2, int i) {
		double hours = 0;
		double mins = 0;

		System.out.println(duration2);
		int indexOfHours = 0;

		int indexOfHours1 = duration2.indexOf("godz.");
		int indexOfHours2 = duration2.indexOf("hour");

		if (indexOfHours1 > indexOfHours2) {
			indexOfHours = indexOfHours1;
		} else {
			indexOfHours = indexOfHours2;
		}

		if (indexOfHours > 0) {
			String tmp = duration2.substring(0, indexOfHours).trim();

			hours = Integer.valueOf(tmp);
		}

		int indexOfMins = 0;

		int indexOfMins1 = duration2.indexOf("mins");
		int indexOfMins2 = duration2.indexOf("min");

		if (indexOfMins1 > indexOfMins2) {
			indexOfMins = indexOfMins1;
		} else {
			indexOfMins = indexOfMins2;
		}

		if (indexOfMins > 0) {

			if (indexOfHours > 0) {
				String tmp = duration2.substring(indexOfHours + 6, indexOfMins)
						.trim();
				mins = Integer.valueOf(tmp);
			} else {
				String tmp = duration2.substring(0, indexOfMins).trim();
				mins = Integer.valueOf(tmp);
			}

		}
		if (i == 0) {
			return hours;
		} else {
			return mins;
		}

	}

	/**
	 * Method to decode polyline points Courtesy :
	 * jeffreysambells.com/2010/05/27
	 * /decoding-polylines-from-google-maps-direction-api-with-java
	 * */
	private List<LatLng> decodePoly(String encoded) {

		List<LatLng> poly = new ArrayList<LatLng>();
		int index = 0, len = encoded.length();
		int lat = 0, lng = 0;

		while (index < len) {
			int b, shift = 0, result = 0;
			do {
				b = encoded.charAt(index++) - 63;
				result |= (b & 0x1f) << shift;
				shift += 5;
			} while (b >= 0x20);
			int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
			lat += dlat;

			shift = 0;
			result = 0;
			do {
				b = encoded.charAt(index++) - 63;
				result |= (b & 0x1f) << shift;
				shift += 5;
			} while (b >= 0x20);
			int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
			lng += dlng;

			LatLng p = new LatLng((((double) lat / 1E5)),
					(((double) lng / 1E5)));
			poly.add(p);
		}
		return poly;
	}
}
