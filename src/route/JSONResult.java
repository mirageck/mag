package route;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class JSONResult {

	private List<List<HashMap<String, Double>>> routes = new ArrayList<List<HashMap<String, Double>>>();
	private List<List<HashMap<String, String>>> routesString = new ArrayList<List<HashMap<String, String>>>();
	private List<List<HashMap<String, Double>>> routesPoints = new ArrayList<List<HashMap<String, Double>>>();

	public List<List<HashMap<String, Double>>> getRoutes() {
		return routes;
	}

	public void setRoutes(List<List<HashMap<String, Double>>> routes) {
		this.routes = routes;
	}

	public List<List<HashMap<String, String>>> getRoutesString() {
		return routesString;
	}

	public void setRoutesString(List<List<HashMap<String, String>>> routes2) {
		this.routesString = routes2;
	}

	public void setAllRoutes(List<List<HashMap<String, Double>>> routes,
			List<List<HashMap<String, String>>> routes2,
			List<List<HashMap<String, Double>>> routesPoints) {
		this.routes = routes;
		this.routesString = routes2;
		this.setRoutesPoints(routesPoints);

		// TODO Auto-generated method stub

	}

	public List<List<HashMap<String, Double>>> getRoutesPoints() {
		return routesPoints;
	}

	public void setRoutesPoints(List<List<HashMap<String, Double>>> routesPoints) {
		this.routesPoints = routesPoints;
	}

}
