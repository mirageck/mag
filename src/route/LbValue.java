package route;

public class LbValue {

	private int x;
	private int y;
	private int value;
	private double max;

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public LbValue(int x, int y, int value, double max) {
		super();
		this.x = x;
		this.y = y;
		this.value = value;
		this.max = max;
	}

	public LbValue() {
		super();
		this.x = 0;
		this.y = 0;
		this.value = 0;
		this.max = 0;
	}

	public double getMax() {
		return max;
	}

	public void setMax(double max) {
		this.max = max;
	}

}
