package route;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import parking.ParkingDatabase;
import place.Place;
import place.PlaceList;
import utility.Alert;
import android.app.Activity;
import android.content.Context;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.mag.R;

public class Gantt {

	public PlaceList placeList;
	private ParkingDatabase parkingDatabes;
	private int maxDistanceToParking = 0;
	public Distance tmp;
	private GoogleMap map;
	public BestList best = new BestList();
	public List<Polyline> routes = new ArrayList<Polyline>();
	public List<Marker> points = new ArrayList<Marker>();
	public List<Marker> parkings = new ArrayList<Marker>();
	public DistanceMap placeDistanceMap = new DistanceMap();
	private int listNumber;
	Activity activity;
	Context context;
	boolean isSimulate = false;
	Date simulationDate;
	GoogleDirections googleDirection = new GoogleDirections();
	DistanceMethods distanceMethods = new DistanceMethods();
	BranchAndBound branchAndBound;
	Place myPlace;
	private boolean work = true;
	private boolean realDistances = false;

	public int getListNumber() {
		return listNumber;
	}

	public void setListNumber(int listNumber) {
		this.listNumber = listNumber;
	}

	public Gantt(PlaceList placeList, ParkingDatabase parkingDatabes,
			int distance, GoogleMap map, int listNumber, Activity activity) {

		this.placeList = placeList;
		this.parkingDatabes = parkingDatabes;
		this.maxDistanceToParking = distance;
		this.map = map;
		this.listNumber = listNumber;
		this.activity = activity;
		branchAndBound = new BranchAndBound(best, parkingDatabes,
				maxDistanceToParking, placeList, activity);

	}

	public void showParkings() {

		for (Place place : placeList.getList(listNumber)) {

			if (place.isParking()) {
				parkings.add(place.getParkingPlace().showMarker(map));

			}
		}
	}

	public void showPlaces() {

		for (Place place : placeList.getList(listNumber)) {
			if (!place.getName().equals("Moja Lokalizacja")) {
				place.marker.setVisible(true);
			}
		}
	}

	private boolean checkPrev(Distance distance, int index) {

		boolean not = true;

		for (Distance tmp : best.getList(index)) {

			if (tmp.getFrom().getName().equals(distance.getTo().getName())) {
				not = false;
				break;
			}

		}
		return not;
	}

	public void addMyLocation(int index, int op) {

		if (!placeList.isMyLocation(index)) {

			if (op == 0) {

				if (map.isMyLocationEnabled()) {
					myPlace = new Place("Moja Lokalizacja", 0, 0);
					myPlace.getLocation().setLatitude(
							map.getMyLocation().getLatitude());
					myPlace.getLocation().setLongitude(
							map.getMyLocation().getLongitude());

					placeList.add(index, myPlace);
					placeList.setMyLocation(index, true);
				}
			} else {
				placeList.add(index, myPlace);
				placeList.setMyLocation(index, true);

			}
		}
	}

	public void getFastRoute(final int index) {

		new Thread(new Runnable() {
			public void run() {

				clearAllRoutes();
				distanceMethods.getDistanceBettwenPlaces(index,
						placeDistanceMap, placeList, realDistances);

				best.clear(index);
				ArrayList<Distance> tmp = placeDistanceMap.get(index,
						"Moja Lokalizacja");

				best.add(index, tmp.get(0));
				googleDirection.calculateDistance(best.get(index, 0), work);
				best.getLast(index).createStartTimes(false);
				for (int i = 1; i < placeDistanceMap.size(index) - 1; i++) {

					tmp = placeDistanceMap.get(index, best.getLast(index)
							.getTo().getName());

					for (Distance distance : tmp) {

						if (checkPrev(distance, index)) {
							best.add(index, distance);
							googleDirection.calculateDistance(distance, work);
							best.getLast(index).createStartTimes(false);

							break;
						}

					}

				}

				ArrayList<Distance> last = placeDistanceMap.get(index, best
						.getLast(index).getTo().getName());

				for (Distance distance : last) {

					if (distance.getTo().getName().equals("Moja Lokalizacja")) {

						best.add(index, distance);
						googleDirection.calculateDistance(best.getLast(index),
								work);
						best.getLast(index).createStartTimes(false);

						break;
					}
				}

				placeList.eliminate(index);
			}
		}).start();
	}

	public void getFastRouteIncludeParkings(final int index) {

		new Thread(new Runnable() {
			public void run() {
				Date start = new Date();
				clearAllRoutes();
				distanceMethods.getDistanceBettwenPlaces(index,
						placeDistanceMap, placeList, realDistances);
				best.clear(index);
				ArrayList<Distance> tmp = placeDistanceMap.get(index,
						"Moja Lokalizacja");

				for (int i = 0; i < placeDistanceMap.size(index) - 1; i++) {
					if (i == 0) {
					} else {
						if (best.size(index) > 0) {
							tmp = placeDistanceMap.get(index,
									best.getLast(index).getTo().getName());
						}
					}
					for (Distance distance : tmp) {

						if (checkPrev(distance, index)) {

							best.add(index, distance);
							googleDirection.calculateDistance(distance, work);
							best.getLast(index).createStartTimes(false);

							if (parkingDatabes.search(distance.getTo(),
									maxDistanceToParking)) {
								distance.getTo().difStartTimeParkingTime();
								best.getLast(index).createStartTimes(true);

								break;

							} else {
								best.remove(index, best.size(index) - 1);
							}
						}
					}

					if (best.size(index) < 1) {
						best.add(index, tmp.get(0));
					}

				}
				int size = placeDistanceMap.size(index) - best.size(index);
				for (int i = 0; i < size; i++) {
					tmp = placeDistanceMap.get(index, best.getLast(index)
							.getTo().getName());

					for (Distance distance : tmp) {

						if (checkPrev(distance, index)) {
							best.add(index, distance);
							googleDirection.calculateDistance(distance, work);
							best.getLast(index).createStartTimes(false);
							if (parkingDatabes.search(distance.getTo(),
									maxDistanceToParking)) {
								distance.getTo().difStartTimeParkingTime();

								best.getLast(index).createStartTimes(true);
							}
							break;
						}
					}
				}

				best.setStartDate(index);

				ArrayList<Distance> last = placeDistanceMap.get(index, best
						.getLast(index).getTo().getName());

				for (Distance distance : last) {

					if (distance.getTo().getName().equals("Moja Lokalizacja")) {

						best.add(index, distance);
						googleDirection.calculateDistance(best.getLast(index),
								work);
						best.getLast(index).createStartTimes(false);

						break;
					}
				}
				best.setEndDate(index);

				placeList.eliminate(index);

				Date end = new Date();

				long diff = end.getTime() - start.getTime();

				long seconds = TimeUnit.MILLISECONDS.toSeconds(diff);

				best.setTimes(seconds, index);
				showToast("Trasa numer: " + index + " opracowana, czas: "
						+ seconds + "s");
			}

		}).start();
	}

	public void showRoad() {

		for (int i = 0; i < best.size(listNumber); i++) {

			if (i == best.size(listNumber) - 1) {

			} else {

				best.get(listNumber, i).getTo().getMarker()
						.setSnippet("" + (i + 1));
			}

			routes.add(map.addPolyline(best.get(listNumber, i).getRoute()));

		}
	}

	public void showInstructions() {

		for (Distance distance : best.getList(listNumber)) {

			Place p = distance.getTo();

			for (int x = 0; x < p.getNavigationData().getNaviPoints().size(); x++) {

				MarkerOptions options = new MarkerOptions()
						.title("Wskaz�wka")
						.position(p.getNavigationData().getNaviPoints().get(x))
						.snippet(
								p.getNavigationData().getNaviInstruction()
										.get(x))
						.icon(BitmapDescriptorFactory
								.fromResource(R.drawable.point));

				boolean tmp = false;

				for (Marker point : points) {

					if (options.getPosition().equals(point.getPosition())) {

						point.setSnippet(point.getSnippet()
								+ "\n"
								+ "\n"
								+ p.getNavigationData().getNaviInstruction()
										.get(x));
						tmp = true;
						break;
					}
				}
				if (!tmp) {
					points.add(map.addMarker(options));
				}
			}

		}
	}

	public void hideRoad() {
		routes.clear();
		points.clear();
		parkings.clear();

	}

	public void clearAllRoutes() {

		for (Polyline route : routes) {

			route.remove();

		}

	}

	public void calcualteFiveRoutes(int max, boolean searchParkings, int alg) {

		for (int i = 0; i < max; i++) {

			if (placeList.size(i) == 0) {

				best.clear(i);

			}

			if (best.size(i) > 0) {

				hideRoad();
				addMyLocation(i, 1);
				corretRoute(i, getSimDate());
			}
			if (best.size(i) == 0 && placeList.size(i) > 0) {

				if (alg == 0) {
					addMyLocation(i, 0);

					if (searchParkings) {
						hideRoad();
						getFastRouteIncludeParkings(i);
					} else {
						hideRoad();
						getFastRoute(i);
					}

				} else {

					addMyLocation(i, 0);

					if (searchParkings) {
						hideRoad();
						branchAndBound.calculateRouteIncludeParkings(i);

					} else {
						hideRoad();
						branchAndBound.calculateRoute(i);

					}

				}
				// drugi alg

			}
		}

	}

	public void showToast(final String toast) {
		activity.runOnUiThread(new Runnable() {
			public void run() {

				Alert alert = new Alert();

				alert.showFinishDialog(activity, "Informacja", toast, false);

				// Toast.makeText(activity, toast, Toast.LENGTH_SHORT).show();
			}
		});
	}

	public void checkBetterParking(int user, int i) {

		if (parkingDatabes.update(best.get(user, i).getTo(),
				maxDistanceToParking)) {
			best.get(user, i).createStartTimes(true);
		}

	}

	public void corretRoute(int index, Date date) {

		clearAllRoutes();
		distanceMethods.getDistanceBettwenPlaces(index, placeDistanceMap,
				placeList, realDistances);
		getCorrectedFastRouteIncludeParkings(index, date);

	}

	private Thread getCorrectedFastRouteIncludeParkings(final int index,
			final Date date) {

		int x = best.size(index) - 1;
		for (int i = 0; i < best.size(index); i++) {

			if (best.get(index, i).getFrom().getStartActivityDateEnd()
					.before(date)
					&& best.get(index, i).getTo().getStartActivityDateEnd()
							.after(date)) {

				x = i;
				break;
			}
		}
		if (x + 1 < best.size(index)) {
			for (int i = best.size(index) - 1; i > x; i--) {
				System.out.println("papa");
				best.remove(index, i);

			}
		}

		Thread t = new Thread(new Runnable() {
			@Override
			public void run() {

				Date start = new Date();
				if (best.getLast(index).getTo().getName().equals("Moja Lokalizacja")) {
					best.remove(index, best.size(index) - 1);
				}

				int size = placeDistanceMap.size(index) - best.size(index);

				for (int i = 0; i < size; i++) {

					ArrayList<Distance> tmp = placeDistanceMap.get(index, best
							.getLast(index).getTo().getName());

					for (Distance distance : tmp) {

						if (checkPrev(distance, index)) {

							best.add(index, distance);
							googleDirection.calculateDistance(distance, work);

							best.getLast(index).createStartTimes(false, date);

							if (parkingDatabes.search(distance.getTo(),
									maxDistanceToParking)) {
								distance.getTo().difStartTimeParkingTime();

								best.getLast(index)
										.createStartTimes(true, date);
								break;
							} else {
								best.remove(index, best.size(index) - 1);

							}

						}

					}

				}
				size = placeDistanceMap.size(index) - best.size(index);

				for (int i = 0; i < size; i++) {

					ArrayList<Distance> tmp = placeDistanceMap.get(index, best
							.getLast(index).getTo().getName());

					for (Distance distance : tmp) {

						if (checkPrev(distance, index)) {

							best.add(index, distance);
							googleDirection.calculateDistance(distance, work);
							best.getLast(index).createStartTimes(false, date);
							break;
						}

					}

				}
				ArrayList<Distance> last = placeDistanceMap.get(index, best
						.getLast(index).getTo().getName());

				for (Distance distance : last) {

					if (distance.getTo().getName().equals("Moja Lokalizacja")) {

						best.add(index, distance);
						googleDirection.calculateDistance(best.getLast(index),
								work);
						best.getLast(index).createStartTimes(false);

						break;
					}
				}

				placeList.eliminate(index);
				Date end = new Date();

				long diff = end.getTime() - start.getTime();

				long seconds = TimeUnit.MILLISECONDS.toSeconds(diff);
				showToast("Trasa numer: " + index + " poprawiona w czasie: "
						+ seconds + "s");
			}

		});
		t.start();
		return t;
	}

	public void setSimDate(boolean tmp, Date simulationDate) {
		isSimulate = tmp;
		this.simulationDate = simulationDate;

	}

	public Date getSimDate() {

		if (isSimulate) {
			return simulationDate;
		} else {
			return new Date();
		}
	}

	public ArrayList<Distance> getBest(int index) {
		return best.getList(index);
	}

	public BestList getBest() {
		return best;
	}

}
