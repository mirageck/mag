package parking;

import java.util.Calendar;
import java.util.Date;

import place.Place;


public class SearchResult {

	private int index = 0;
	private double nerest = 9999;
	private boolean free = false;
	private Date bestDate;

	public SearchResult() {

		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());

		cal.add(Calendar.HOUR, +24);
		setBestDate(cal.getTime());

	}

	public SearchResult(Place place) {
		
		this.nerest=place.getDistanceToParking();
		this.bestDate=place.getParkingDate();
		// TODO Auto-generated constructor stub
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public double getNerest() {
		return nerest;
	}

	public void setNerest(double nerest) {
		this.nerest = nerest;
	}

	public Date getBestDate() {
		return bestDate;
	}

	public void setBestDate(Date bestDate) {
		this.bestDate = bestDate;
	}

	public boolean isFree() {
		return free;
	}

	public void setFree(boolean free) {
		this.free = free;
	}

}
