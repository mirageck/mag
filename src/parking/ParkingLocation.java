package parking;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import place.Place;
import time.MyDate;
import android.annotation.SuppressLint;
import android.location.Location;

public class ParkingLocation {

	private List<MyDate> reservationList = Collections
			.synchronizedList(new ArrayList<MyDate>());
	private LatLng coordinates;

	public ParkingLocation(double lat, double lng) {

		this.coordinates = new LatLng(lat, lng);
		// TODO Auto-generated constructor stub
	}

	@SuppressLint("SimpleDateFormat")
	public boolean addReservation(String name, MyDate tmp) {

		tmp.setName(name);

		reservationList.add(tmp);

		sortList();

		return true;

	}

	public void sortList() {
		Collections.sort(reservationList, new Comparator<MyDate>() {

			@Override
			public int compare(MyDate arg0, MyDate arg1) {
				// TODO Auto-generated method stub

				return arg0.getTimeStart().compareTo(arg1.getTimeStart());
			}
		});
	}

	public void removeReservation(MyDate tmp) {
		for (int i = 0; i < reservationList.size(); i++) {
			if (reservationList.get(i).getTimeStart()
					.compareTo(tmp.getTimeStart()) == 0) {
				reservationList.remove(i);
			}
		}
		sortList();
	}

	public MyDate getReservation(int i) {

		return reservationList.get(i);

	}

	public boolean findFirstPossible(Place place, MyDate searchTime) {
		Date tmp_start = searchTime.getTimeStart();
		if (reservationList.size() == 0) {
			place.setParkingDate(searchTime.getTimeStart());
			return true;
		}
		if (reservationList.size() == 1) {
			if (searchTime.getTimeEnd().before(
					(reservationList.get(0).getTimeStart()))) {
				place.setParkingDate(searchTime.getTimeStart());
				return true;
			}
			if (searchTime.getTimeStart().after(
					(reservationList.get(0).getTimeEnd()))) {
				place.setParkingDate(searchTime.getTimeStart());
				return true;
			} else {
				searchTime.changeStartTime(reservationList.get(0).getTimeEnd());
				if (searchTime.ifNotTooLate(tmp_start)) {
					place.setParkingDate(reservationList.get(0).getTimeEnd());
					return true;
				} else {
					searchTime.changeStartTime(tmp_start);
					return false;
				}
			}
		}
		if (reservationList.size() > 1) {
			if (reservationList.get(0).getTimeStart()
					.after(searchTime.getTimeEnd())) {
				place.setParkingDate(searchTime.getTimeStart());
				return true;
			}
			for (int i = 0; i < reservationList.size() - 1; i++) {
				if (searchTime.getTimeStart().after(
						reservationList.get(i).getTimeEnd())
						&& searchTime.getTimeEnd().before(
								reservationList.get(i + 1).getTimeStart())) {
					place.setParkingDate(searchTime.getTimeStart());
					return true;
				} else {
					searchTime.changeStartTime(reservationList.get(i)
							.getTimeEnd());
					if (searchTime.getTimeEnd().before(
							reservationList.get(i + 1).getTimeStart())
							&& searchTime.ifNotTooLate(tmp_start)) {
						place.setParkingDate(searchTime.getTimeStart());
						searchTime.changeStartTime(tmp_start);
						return true;
					} else {
						searchTime.changeStartTime(tmp_start);
					}
				}
			}
			searchTime.changeStartTime(reservationList.get(
					reservationList.size() - 1).getTimeEnd());
			if (searchTime.ifNotTooLate(tmp_start)) {
				place.setParkingDate(searchTime.getTimeStart());
				searchTime.changeStartTime(tmp_start);
				return true;
			} else {
				searchTime.changeStartTime(tmp_start);
			}
		}
		return false;
	}

	public int getSize() {
		return reservationList.size();
	}

	public double getLatitude() {
		return coordinates.latitude;

	}

	public double getLongitude() {
		return coordinates.longitude;

	}

	public Location getLocation() {

		Location location = new Location("parking");
		location.setLatitude(coordinates.latitude);
		location.setLongitude(coordinates.longitude);
		return location;
	}

	public void removeReservation(Date parkingDate) {

		MyDate tmp = new MyDate(parkingDate, 0, 0);

		for (int i = 0; i < reservationList.size(); i++) {
			if (reservationList.get(i).getTimeStart()
					.compareTo(tmp.getTimeStart()) == 0) {
				reservationList.remove(i);
			}
		}
		sortList();
	}

	public List<MyDate> getReservationList() {

		return reservationList;
	}

	public boolean findFirstPossible(MyDate pp) {
		Date tmp_start = pp.getTimeStart();
		if (reservationList.size() == 0) {
			return true;
		}
		if (reservationList.size() == 1) {
			if (pp.getTimeEnd().before((reservationList.get(0).getTimeStart()))) {
				return true;
			}
			if (pp.getTimeStart().after((reservationList.get(0).getTimeEnd()))) {
				return true;
			} else {
				pp.changeStartTime(reservationList.get(0).getTimeEnd());
				if (pp.ifNotTooLate(tmp_start)) {
					return true;
				} else {
					return false;
				}
			}
		}
		if (reservationList.size() > 1) {
			if (reservationList.get(0).getTimeStart().after(pp.getTimeEnd())) {
				return true;
			}
			for (int i = 0; i < reservationList.size() - 1; i++) {
				if (pp.getTimeStart()
						.after(reservationList.get(i).getTimeEnd())
						&& pp.getTimeEnd().before(
								reservationList.get(i + 1).getTimeStart())) {
					return true;
				} else {
					pp.changeStartTime(reservationList.get(i).getTimeEnd());

					if (pp.getTimeEnd().before(
							reservationList.get(i + 1).getTimeStart())
							&& pp.ifNotTooLate(tmp_start)) {
						return true;
					} else {
						pp.changeStartTime(tmp_start);
					}
				}
			}
			pp.changeStartTime(reservationList.get(reservationList.size() - 1)
					.getTimeEnd());
			if (pp.ifNotTooLate(tmp_start)) {
				return true;
			} else {
				return false;
			}
		}
		return false;
	}

	public void clearReservationList() {
		// TODO Auto-generated method stub
		reservationList = Collections.synchronizedList(new ArrayList<MyDate>());
	}

	@SuppressLint("SimpleDateFormat")
	public Marker showMarker(GoogleMap map) {
		// TODO Auto-generated method stub
		SimpleDateFormat dt = new SimpleDateFormat("dd-MMM HH:mm a");
		String s = "Rezerawcje: \n ";
		for (int x = 0; x < reservationList.size(); x++) {

			s = s + "Od:" + dt.format(reservationList.get(x).getTimeStart())
					+ " Do: " + dt.format(reservationList.get(x).getTimeEnd())
					+ "\n";

		}

		Marker marker = map.addMarker(new MarkerOptions()
				.title("Parking")
				.icon(BitmapDescriptorFactory
						.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
				.snippet(s).position(coordinates));
		marker.setVisible(true);

		return marker;
	}

}
