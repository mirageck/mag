package parking;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Random;

import place.Place;
import time.MyDate;
import android.content.Context;
import android.util.SparseArray;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import database.DatabaseMethods;

public class ParkingDatabase {

	boolean all = false;
	private int option = 0;
	Context context;

	private SparseArray<ParkingLocation> parkingLocations = new SparseArray<ParkingLocation>();
	private List<Marker> parkings = new ArrayList<Marker>();
	DatabaseMethods baseMethods;

	public ParkingDatabase(Context mainActivity) {
		context = mainActivity;

	}

	public void setOptions(int op) {
		this.option = op;
	}

	public void addParkingLocation(ParkingLocation p) {

		parkingLocations.put(parkingLocations.size(), p);
	}

	public ParkingLocation getParkingLocation(int i) {

		return parkingLocations.get(i);
	}

	public boolean search(Place place, double metry) {

		if (option == 0) {
			return searchFirstNearestParkingLocationThreads(place, metry);
		} else {
			return searchFirstNearestParkingLocation(place, metry);
		}

	}

	private boolean searchFirstNearestParkingLocation(Place place, double metry) {
		boolean free = false;
		int index = 0;
		double nerest = 9999;
		Date bestDate;
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.HOUR, +24);
		bestDate = cal.getTime();
		place.setParking(false);
		MyDate tmp_time = new MyDate(place.getStartActivityDate(), place
				.getPlaceTime().getHours(), place.getPlaceTime().getMinutes());
		synchronized (parkingLocations) {
			for (int i = 0; i < parkingLocations.size(); i++) {
				tmp_time = new MyDate(place.getStartActivityDate(), place
						.getPlaceTime().getHours(), place.getPlaceTime()
						.getMinutes());
				double tmp = place.distanceTo(parkingLocations.get(i)
						.getLocation());
				if (tmp < metry) {
					if (tmp <= nerest) {
						if (parkingLocations.get(i).findFirstPossible(tmp_time)) {
							if (tmp_time.getTimeStart().compareTo((bestDate)) <= 0) {
								bestDate = tmp_time.getTimeStart();
								index = i;
								nerest = tmp;
								free = true;
							}
						}
					}
				}
			}
			if (free) {
				place.setParkingPlace(parkingLocations.get(index));
				place.setDistanceToParking(nerest);
				place.setParkingDate(bestDate);
				place.setParking(true);
				synchronized (parkingLocations.get(index)) {
					place.reservedParking();
				}
				return true;
			} else {
				place.setParking(false);
				return false;
			}
		}
	}

	private boolean searchFirstNearestParkingLocationThreads(Place place,
			double metry) {

		ArrayList<SearchResult> list = new ArrayList<SearchResult>();
		list.add(new SearchResult());
		list.add(new SearchResult());
		list.add(new SearchResult());
		list.add(new SearchResult());

		SearchResult result = new SearchResult();

		ArrayList<Thread> listThread = new ArrayList<Thread>();

		place.setParking(false);

		Thread t1 = search(place, metry, list.get(0), 0,
				(parkingLocations.size() / 4));
		listThread.add(t1);
		Thread t2 = search(place, metry, list.get(1),
				(parkingLocations.size() / 4), (parkingLocations.size() / 2));
		listThread.add(t2);
		Thread t3 = search(place, metry, list.get(2),
				(parkingLocations.size() / 2),
				(parkingLocations.size() / 4) * 3);
		listThread.add(t3);
		Thread t4 = search(place, metry, list.get(3),
				(parkingLocations.size() / 4) * 3, parkingLocations.size());
		listThread.add(t4);

		for (Thread thread : listThread) {

			try {
				thread.join();
			} catch (InterruptedException e) {

				e.printStackTrace();
			}

		}

		sortList(list);

		result = list.get(0);
		removedReservation(list.get(0));
		removedReservation(list.get(1));
		removedReservation(list.get(2));
		removedReservation(list.get(3));

		if (result.isFree()) {
			place.setParkingPlace(parkingLocations.get(result.getIndex()));
			place.setDistanceToParking(result.getNerest());
			place.setParkingDate(result.getBestDate());
			place.setParking(true);

			removeReservation(result.getIndex(), result.getBestDate());
			place.reservedParking();

			return true;
		} else {
			place.setParking(false);
			return false;
		}

	}

	public void sortList(ArrayList<SearchResult> list) {
		Collections.sort(list, new Comparator<SearchResult>() {

			@Override
			public int compare(SearchResult arg0, SearchResult arg1) {
				return arg0.getBestDate().compareTo(arg1.getBestDate());
			}
		});
	}

	private Thread search(final Place place, final double metry,
			final SearchResult result, final int x, final int y) {

		Thread t = new Thread(new Runnable() {
			@Override
			public void run() {

				for (int i = x; i < y; i++) {

					MyDate tmp_time = new MyDate(place.getStartActivityDate(),
							place.getPlaceTime().getHours(), place
									.getPlaceTime().getMinutes());

					double tmp = place.distanceTo(parkingLocations.get(i)
							.getLocation());

					if (tmp < metry) {

						if (tmp < result.getNerest()) {

							if (parkingLocations.get(i).findFirstPossible(
									tmp_time)) {

								if (tmp_time.getTimeStart().compareTo(
										result.getBestDate()) <= 0) {

									if (!result.isFree()) {

									} else {
										removedReservation(result);
									}

									result.setBestDate(tmp_time.getTimeStart());
									result.setIndex(i);
									result.setNerest(tmp);
									result.setFree(true);

									addReservation(result, place);

								}

							}
						}
					}

				}
			}

		});
		t.start();

		return t;
	}

	private void addReservation(SearchResult result, Place place) {

		MyDate tmp_time = new MyDate(result.getBestDate(), place.getPlaceTime()
				.getHours(), place.getPlaceTime().getMinutes());
		parkingLocations.get(result.getIndex()).addReservation("tmp", tmp_time);

	}

	private void removedReservation(SearchResult result) {
		parkingLocations.get(result.getIndex()).removeReservation(
				result.getBestDate());

	}

	private boolean updateParkingThreads(Place place, double metry) {

		ArrayList<SearchResult> list = new ArrayList<SearchResult>();

		if (place.isParking()) {

			list.add(new SearchResult(place));
			list.add(new SearchResult(place));
			list.add(new SearchResult(place));
			list.add(new SearchResult(place));

		} else {

			list.add(new SearchResult());
			list.add(new SearchResult());
			list.add(new SearchResult());
			list.add(new SearchResult());
			place.setParking(false);
		}

		SearchResult result = new SearchResult();

		ArrayList<Thread> listThread = new ArrayList<Thread>();

		Thread t1 = search(place, metry, list.get(0), 0,
				(parkingLocations.size() / 4));
		listThread.add(t1);
		Thread t2 = search(place, metry, list.get(1),
				(parkingLocations.size() / 4), (parkingLocations.size() / 2));
		listThread.add(t2);
		Thread t3 = search(place, metry, list.get(2),
				(parkingLocations.size() / 2),
				(parkingLocations.size() / 4) * 3);
		listThread.add(t3);
		Thread t4 = search(place, metry, list.get(3),
				(parkingLocations.size() / 4) * 3, parkingLocations.size());
		listThread.add(t4);

		for (Thread thread : listThread) {

			try {
				thread.join();
			} catch (InterruptedException e) {

				e.printStackTrace();
			}

		}

		sortList(list);

		result = list.get(0);
		removedReservation(list.get(0));
		removedReservation(list.get(1));
		removedReservation(list.get(2));
		removedReservation(list.get(3));

		if (result.isFree()) {
			place.setParkingPlace(parkingLocations.get(result.getIndex()));
			place.setDistanceToParking(result.getNerest());
			place.setParkingDate(result.getBestDate());

			if (place.isParking()) {
				place.removeReservation();
			} else {
				place.setParking(true);
			}

			removeReservation(result.getIndex(), result.getBestDate());
			place.reservedParking();

			return true;
		} else {
			;
			return false;
		}

	}

	private boolean updateParking(Place place, double metry) {
		boolean free = false;
		int index = 0;
		double nerest = 9999;

		Date bestDate;
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());

		cal.add(Calendar.HOUR, +24);
		bestDate = cal.getTime();

		if (place.isParking()) {
			nerest = place.getDistanceToParking();
			bestDate = place.getParkingDate();
		} else {
			place.setParking(false);
		}
		MyDate tmp_time = new MyDate(place.getStartActivityDate(), place
				.getPlaceTime().getHours(), place.getPlaceTime().getMinutes());

		synchronized (parkingLocations) {

			for (int i = 0; i < parkingLocations.size(); i++) {

				tmp_time = new MyDate(place.getStartActivityDate(), place
						.getPlaceTime().getHours(), place.getPlaceTime()
						.getMinutes());

				double tmp = place.distanceTo(parkingLocations.get(i)
						.getLocation());

				if (tmp < metry) {

					if (tmp <= nerest) {

						if (parkingLocations.get(i).findFirstPossible(tmp_time)) {

							if (tmp_time.getTimeStart().compareTo(bestDate) <= 0) {

								bestDate = tmp_time.getTimeStart();
								index = i;
								nerest = tmp;
								free = true;

							}

						}
					}
				}

			}
			if (free) {
				place.setParkingPlace(parkingLocations.get(index));
				place.setDistanceToParking(nerest);
				place.setParkingDate(bestDate);

				if (place.isParking()) {
					place.removeReservation();
				} else {
					place.setParking(true);
				}
				synchronized (parkingLocations.get(index)) {

					searchFirstNearestParkingLocation(place, metry);

				}

				return true;
			} else {
				return false;
			}
		}
	}

	public int size() {
		return parkingLocations.size();
	}

	public boolean update(Place place, double metry) {

		if (option == 0) {
			return updateParkingThreads(place, metry);

		} else {
			return updateParking(place, metry);
		}
	}

	public void addReservation(ParkingLocation tmp, Date date, int h, int min) {

		MyDate tmp_time = new MyDate(date, h, min);

		tmp.addReservation("test", tmp_time);

	}

	public void addTestReservation(int tmp, Date date, int h, int min) {

		MyDate tmp_time = new MyDate(date, h, min);

		parkingLocations.get(parkingLocations.size() - tmp - 1).addReservation(
				"test", tmp_time);

	}

	public void addParkingLocation(double latitude, double longitude) {

		ParkingLocation tmp = new ParkingLocation(latitude, longitude);

		parkingLocations.put(parkingLocations.size(), tmp);
	}

	public void allMakers(GoogleMap map) {
		if (all == true) {
			hideAll();
		} else {
			showAll(map);
		}

	}

	public void reservedDay() {
		Date date = new Date();

		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.HOUR, +23);
		date = cal.getTime();

		for (int i = 0; i < parkingLocations.size(); i++) {

			addReservation(parkingLocations.get(i), date, 22, 0);
		}

	}

	private void removeReservation(int index, Date date) {

		parkingLocations.get(index).removeReservation(date);
	}

	public void createTmpReservation() {

		new Thread(new Runnable() {
			public void run() {

				Date date = new Date();
				MyDate tmp;
				Date tmpDate;
				Calendar cal = Calendar.getInstance();

				for (int i = 0; i < parkingLocations.size(); i++) {

					tmpDate = date;
					cal.setTime(tmpDate);

					Random rand = new Random();
					int x = rand.nextInt(3);

					cal.add(Calendar.HOUR, +x);
					tmpDate = cal.getTime();

					tmp = new MyDate(tmpDate, 1, 0);

					parkingLocations.get(i).addReservation("test", tmp);

					x = rand.nextInt(4) + 4;

					cal.add(Calendar.HOUR, +x);
					tmpDate = cal.getTime();

					tmp = new MyDate(tmpDate, 2, 0);

					parkingLocations.get(i).addReservation("test", tmp);

					x = rand.nextInt(2) + 7;

					cal.add(Calendar.HOUR, +x);
					tmpDate = cal.getTime();

					tmp = new MyDate(tmpDate, 1, 0);

					parkingLocations.get(i).addReservation("test", tmp);

				}

			}
		}).start();

	}

	public void showAll(GoogleMap map) {

		for (int i = 0; i < parkingLocations.size(); i++) {
			parkings.add(parkingLocations.get(i).showMarker(map));

		}
		// TODO Auto-generated method stub
		all = true;

	}

	public void hideAll() {
		for (Marker tmp : parkings) {
			tmp.remove();

		}
		parkings.clear();
		all = false;
	}

	// @SuppressLint("SdCardPath")
	// public void readDatabes2() {
	//
	// FileInputStream in = null;
	// Scanner br;
	// boolean newFile = false;
	//
	// try {
	// in = new FileInputStream("/sdcard/parking_database.txt");
	// br = new Scanner(new InputStreamReader(in));
	//
	// } catch (FileNotFoundException e) {
	//
	// InputStream in2 = context.getResources().openRawResource(id);
	//
	// br = new Scanner(new InputStreamReader(in2));
	//
	// newFile = true;
	//
	// e.printStackTrace();
	// }
	// if (newFile) {
	//
	// File myFile = new File("/sdcard/parking_database.txt");
	// System.out.println("nowy plika");
	//
	// try {
	// myFile.createNewFile();
	// } catch (IOException e) {
	//
	// e.printStackTrace();
	// }
	//
	// FileWriter f = null;
	//
	// try {
	// f = new FileWriter("/sdcard/parking_database.txt", true);
	// } catch (IOException e) {
	//
	// e.printStackTrace();
	// }
	//
	// while (br.hasNext()) {
	//
	// String strLine = br.nextLine();
	//
	// int i = strLine.indexOf(",");
	// String slat = strLine.substring(0, i);
	// String slng = strLine.substring(i + 1);
	// double lat = 0;
	// double lng = 0;
	//
	// lat = Double.parseDouble(slat);
	// lng = Double.parseDouble(slng);
	//
	// parkingLocations.put(parkingLocations.size(),
	// new ParkingLocation(lat, lng));
	// try {
	// f.write(lat + "," + lng + "\n");
	// } catch (IOException e) {
	//
	// e.printStackTrace();
	// }
	//
	// }
	//
	// try {
	// f.flush();
	// } catch (IOException e) {
	// e.printStackTrace();
	// }
	// try {
	// f.close();
	// } catch (IOException e) {
	// e.printStackTrace();
	// }
	//
	// } else {
	//
	// while (br.hasNext()) {
	//
	// String strLine = br.nextLine();
	//
	// int i = strLine.indexOf(",");
	// String slat = strLine.substring(0, i);
	// String slng = strLine.substring(i + 1);
	// double lat = 0;
	// double lng = 0;
	//
	// lat = Double.parseDouble(slat);
	// lng = Double.parseDouble(slng);
	//
	// parkingLocations.put(parkingLocations.size(),
	// new ParkingLocation(lat, lng));
	// }
	// }
	//
	// }
	//
	// public void readDatabes() {
	//
	// InputStream in2 = context.getResources().openRawResource(id);
	//
	// Scanner br = new Scanner(new InputStreamReader(in2));
	//
	// while (br.hasNext()) {
	// String strLine = br.nextLine();
	//
	// int i = strLine.indexOf(",");
	// String slat = strLine.substring(0, i);
	// String slng = strLine.substring(i + 1);
	// double lat = 0;
	// double lng = 0;
	//
	// lat = Double.parseDouble(slat);
	// lng = Double.parseDouble(slng);
	//
	// parkingLocations.put(parkingLocations.size(), new ParkingLocation(
	// lat, lng));
	//
	// }
	//
	// }

	public void clearTmpReserv() {
		// TODO Auto-generated method stub
		for (int i = 0; i < parkingLocations.size(); i++) {

			parkingLocations.get(i).clearReservationList();

		}

	}

}
