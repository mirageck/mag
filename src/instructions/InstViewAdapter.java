package instructions;

import java.util.List;

import route.Distance;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mag.R;


public class InstViewAdapter extends BaseExpandableListAdapter {

	private List<Distance> list;
	private Context context;
	private MyIcons icons = new MyIcons();

	public InstViewAdapter(Context context, List<Distance> list) {
		super();
		// TODO Auto-generated constructor stub
		this.list = list;
		this.context = context;

	}

	@Override
	public int getGroupCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		// TODO Auto-generated method stub
		return list.get(groupPosition).getTo().getNavigationData().getNaviDistance().size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		// TODO Auto-generated method stub
		return list.get(groupPosition);
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return list.get(groupPosition).getTo().getNavigationData().getNaviDistance().get(childPosition);
	}

	@Override
	public long getGroupId(int groupPosition) {
		// TODO Auto-generated method stub
		return groupPosition;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return childPosition;
	}

	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if (convertView == null) {

			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.row_inst_layout, parent,
					false);
		}
		TextView tv = (TextView) convertView.findViewById(R.id.textInstName);

		Distance p = list.get(groupPosition);

		tv.setText(p.getFrom().getName() + "-" + p.getTo().getName());

		return convertView;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if (convertView == null) {

			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.row_inst_det_layout,
					parent, false);
		}
		TextView tv = (TextView) convertView
				.findViewById(R.id.textViewInstName);
		TextView tv2 = (TextView) convertView
				.findViewById(R.id.textViewInstDistance);
		TextView tv3 = (TextView) convertView
				.findViewById(R.id.textViewInstDuration);
		TextView tv4 = (TextView) convertView.findViewById(R.id.textViewInstM);
		ImageView img = (ImageView) convertView.findViewById(R.id.imageView1);

		String tmp = list.get(groupPosition).getTo().getNavigationData().getNaviInstruction()
				.get(childPosition);

		tv.setText(tmp);
		tv2.setText(list.get(groupPosition).getTo().getNavigationData().getNaviDistance()
				.get(childPosition));
		tv3.setText(list.get(groupPosition).getTo().getNavigationData().getNaviDuration()
				.get(childPosition));
		String key = list.get(groupPosition).getTo().getNavigationData().getNaviManeuvers()
				.get(childPosition);
		key = key.trim();
		tv4.setText(key);

		img.setImageResource(icons.getId(key));

		return convertView;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return true;
	}

}
