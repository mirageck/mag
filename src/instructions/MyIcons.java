package instructions;

import java.util.HashMap;

import com.mag.R;


public class MyIcons {

	private HashMap<String, Integer> id = new HashMap<String, Integer>();

	public MyIcons() {

		id.put("ramp-left", R.drawable.ramp_left);

		id.put("ramp-right", R.drawable.ramp_right);

		id.put("roundabout-left", R.drawable.roundbout_left);

		id.put("roundabout-right", R.drawable.roundbout_right);

		id.put("straight", R.drawable.straight);

		id.put("turn-left", R.drawable.turn_left);

		id.put("turn-slight-right", R.drawable.turn_slight_right);

		id.put("turn-right", R.drawable.turn_right);

		id.put("turn-sharp-left", R.drawable.turn_sharp_left);

		id.put("turn-sharp-right", R.drawable.turn_sharp_right);

		id.put("turn-slight-left", R.drawable.turn_slight_left);

		id.put("uturn-left", R.drawable.unturn_left);

		id.put("uturn-right", R.drawable.unturn_right);
		id.put("uturn", R.drawable.unturn_right);
		id.put("keep-right", R.drawable.keep_right);
		id.put("keep-left", R.drawable.keep_left);
	}

	public int getId(String key) {

	//	System.out.println(id.size());
		System.out.println(key);
		
	//	try {

			return id.get(key);
			

	//	} catch (NullPointerException e1) {
		
	//		e1.printStackTrace();
	//	}
		// return R.drawable.ramp_left;
	

	}

}
