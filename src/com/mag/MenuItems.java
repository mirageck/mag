package com.mag;

import android.view.MenuItem;

public class MenuItems {
	
	private MenuItem item_gantt;
	private MenuItem item_route;
	private MenuItem item_simulate;
	
	public void setAll(MenuItem menuItemGantt, MenuItem menuItemRoute, MenuItem menuItemSimulate){
		
		item_gantt=menuItemGantt;
		item_gantt.setEnabled(false);
		item_gantt.setVisible(false);
		item_route=menuItemRoute;
		item_route.setEnabled(false);
		item_route.setVisible(false);
		item_simulate=menuItemSimulate;
		item_simulate.setEnabled(false);
		item_simulate.setVisible(false);
		
		
		
	}
	
	public void enableAll(){
		
		item_gantt.setEnabled(true);
		item_gantt.setVisible(true);
		item_route.setEnabled(true);
		item_route.setVisible(true);
		item_simulate.setEnabled(true);
		item_simulate.setVisible(true);
		
	}
	
	public void disableAll(){
		
		item_gantt.setEnabled(false);
		item_gantt.setVisible(false);
		item_route.setEnabled(false);
		item_route.setVisible(false);
		item_simulate.setEnabled(false);
		item_simulate.setVisible(false);
		
	}
	

}
