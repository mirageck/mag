package com.mag;

import java.util.ArrayList;
import java.util.List;

import parking.ParkingDatabase;

import com.google.android.gms.maps.GoogleMap;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;
import android.widget.ToggleButton;
import android.widget.ViewFlipper;

public class MySettings {

	Spinner spinner;
	EditText city_text;
	EditText setting_view;
	SeekBar distance_bar;
	Context context;
	ToggleButton parkingSerch;
	ToggleButton hybrid;
	ToggleButton traffic;
	ToggleButton threads;
	ToggleButton geoService;
	ToggleButton alg;
	private int distance = 250;
	private String city;
	private boolean serchForParkings;
	GoogleMap map;
	private ParkingDatabase parkingDatabes;
	private boolean service = true;
	private int alghoritm=0;

	public MySettings(ViewFlipper vf, GoogleMap map, Context context,
			ParkingDatabase parkingDatabes) {

		this.parkingDatabes = parkingDatabes;
		this.map = map;
		this.context = context;
		spinner = (Spinner) vf.findViewById(R.id.editTestSetUsers);
		city_text = (EditText) vf.findViewById(R.id.setting_city);
		distance_bar = (SeekBar) vf.findViewById(R.id.setting_distance);
		setting_view = (EditText) vf.findViewById(R.id.text_distance);
		parkingSerch = (ToggleButton) vf
				.findViewById(R.id.setting_parking_search);

		hybrid = (ToggleButton) vf.findViewById(R.id.setting_hybrid);

		traffic = (ToggleButton) vf.findViewById(R.id.setting_traffic);

		threads = (ToggleButton) vf.findViewById(R.id.setting_threads);

		geoService = (ToggleButton) vf.findViewById(R.id.setting_service);

		alg = (ToggleButton) vf.findViewById(R.id.setting_alg);

		city_text.setText("Kielce");
		city = "Kielce";
		setting_view.setText(Integer.toString(distance));
		distance_bar.setProgress(distance);

		serchForParkings = true;
		parkingSerch.setChecked(true);
		hybrid.setChecked(true);
		traffic.setChecked(false);
		threads.setChecked(true);
		geoService.setChecked(true);
		alg.setChecked(true);
		
		
		setListner();

		List<Integer> listPlaceListNumber = new ArrayList<Integer>();

		for (int i = 1; i < 6; i++) {
			listPlaceListNumber.add(i);
		}

		ArrayAdapter<Integer> spinerAdapter = new ArrayAdapter<Integer>(
				context, android.R.layout.simple_spinner_item,
				listPlaceListNumber);

		spinerAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		spinner.setAdapter(spinerAdapter);

	}

	public void setListner() {

		hybrid.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (hybrid.isChecked()) {

					map.setMapType(GoogleMap.MAP_TYPE_HYBRID);
				} else {

					map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
				}
			}
		});

		traffic.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (traffic.isChecked()) {

					map.setTrafficEnabled(true);
				} else {

					map.setTrafficEnabled(false);
				}
			}
		});

		parkingSerch.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (parkingSerch.isChecked()) {

					serchForParkings = true;
				} else {

					serchForParkings = false;
				}
			}
		});

		threads.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (threads.isChecked()) {

					parkingDatabes.setOptions(0);

				} else {

					parkingDatabes.setOptions(1);
				}
			}
		});

		geoService.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (geoService.isChecked()) {

					service = true;

				} else {

					service = false;
				}
			}
		});

		alg.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (alg.isChecked()) {

					setAlghoritm(0);

				} else {

					setAlghoritm(1);
				}
			}
		});

		city_text.addTextChangedListener(new TextWatcher() {

			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				city = city_text.getText().toString();
			}
		});

		distance_bar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				distance = progress;

			}

			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
			}

			public void onStopTrackingTouch(SeekBar seekBar) {
				setting_view.setText(Integer.toString(distance));
			}
		});
	}

	public int getDistance() {
		return distance;
	}

	public void setDistance(int distance) {
		this.distance = distance;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
		city_text.setText(city);
	}

	public boolean isSerchForParkings() {
		return serchForParkings;
	}

	public void setSerchForParkings(boolean serchForParkings) {
		this.serchForParkings = serchForParkings;
	}

	public int getUsers() {

		return (Integer) spinner.getSelectedItem();
		// return Integer.parseInt(users_text.getText().toString());
	}

	public boolean service() {
		// TODO Auto-generated method stub
		return service;
	}

	public int getAlghoritm() {
		return alghoritm;
	}

	public void setAlghoritm(int alghoritm) {
		this.alghoritm = alghoritm;
	}

}
