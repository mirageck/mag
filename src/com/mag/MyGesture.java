package com.mag;

import com.mag.MainActivity.Gesture;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.HorizontalScrollView;

public class MyGesture {

	GestureDetector gestrue;

	public MyGesture(Context context, Gesture gesture) {

		gestrue = new GestureDetector(context, gesture);

	}

	public void setDoubleTap(HorizontalScrollView ll) {

		ll.setOnTouchListener(new OnTouchListener() {

			@SuppressLint("ClickableViewAccessibility")
			@Override
			public boolean onTouch(View v, MotionEvent event) {

				gestrue.onTouchEvent(event);

				return false;
			}
		});
	}
}
