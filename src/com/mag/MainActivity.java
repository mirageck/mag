package com.mag;

import geocoding.GeocodingTask;
import geocoding.MyService;
import geocoding.ServiceMethods;
import instructions.InstViewAdapter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import parking.ParkingDatabase;
import place.AddPlaceAdapter;
import place.GoogleNavi;
import place.Place;
import place.PlaceAdapter;
import place.PlaceList;
import place.TestPlace;
import route.Distance;
import route.Gantt;
import utility.Alert;
import utility.ConnectionDetector;
import utility.Spinners;
import utility.SpinnersMethods;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.location.Address;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import database.Database;
import database.DatabaseMethods;

public class MainActivity extends Activity {

	GoogleMap map;
	ViewFlipper vf;
	ViewFlipper ganttViewFlipper;
	PlaceAdapter placeListAdapter;
	AddPlaceAdapter addPlaceAdapter;
	GanttViewAdapter ganntAdapter;
	PlaceList placeList = new PlaceList();
	List<Address> addressList = new ArrayList<Address>();
	ParkingDatabase parkingDatabes;
	AutoCompleteTextView placeNameEditText;
	ListViews listViews;
	ConnectionDetector cd;
	Alert alert = new Alert();
	Spinners spinners;
	MapClicker clicker;
	MySettings appSettings;
	Gantt gantt;
	Distance test;
	MenuItems menuItems = new MenuItems();
	Date startDate = new Date();
	int listNumber = 0;
	MyGesture myGestrue;
	GeocodingTask geoTask;
	SimulationThread simulation;
	TestPlace testPlace;
	InstViewAdapter instructionAdapter;
	ServiceMethods servicesM;
	Intent intent;
	Statistic statistic;
	DatabaseMethods baseMethods;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.viewflipper);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map))
				.getMap();

		if (map == null) {

			Toast.makeText(this, "NIE MA MAPY", Toast.LENGTH_LONG).show();
		}

		vf = (ViewFlipper) findViewById(R.id.view_flipper);

		ganttViewFlipper = (ViewFlipper) findViewById(R.id.ganntChartsFlipper);
		spinners = new Spinners(findViewById(R.id.spinner1),
				findViewById(R.id.spinner2),
				findViewById(R.id.placeListNumberSpinner));

		placeNameEditText = (AutoCompleteTextView) findViewById(R.id.place_name);
		listViews = new ListViews(findViewById(R.id.listView1),
				findViewById(R.id.listView2),
				findViewById(R.id.ganttAdapterList),
				findViewById(R.id.listInst));

		map.setMyLocationEnabled(true);
		map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(52.142,
				21.031), 6));
		map.setMapType(GoogleMap.MAP_TYPE_HYBRID);
		map.setInfoWindowAdapter(new CustomInfoAdapter(MainActivity.this));

		placeNameEditText.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI);
		placeListAdapter = new PlaceAdapter(placeList.getList(listNumber), this);
		addPlaceAdapter = new AddPlaceAdapter(addressList, this);

		ganntAdapter = new GanttViewAdapter(placeList.getList(listNumber),
				this, startDate, ganttViewFlipper, listNumber);

		listViews.getListViewPlace().setAdapter(placeListAdapter);
		listViews.getListViewAddPlace().setAdapter(addPlaceAdapter);
		listViews.getListViewGanttAdapter().setAdapter(ganntAdapter);

		registerForContextMenu(listViews.getListViewPlace());

		cd = new ConnectionDetector(this);
		cd.checkInternet();

		SpinnersMethods myspin = new SpinnersMethods(MainActivity.this,
				android.R.layout.simple_spinner_item, spinners);

		myGestrue = new MyGesture(MainActivity.this, new Gesture());
		addListenerOnButtons();
		Database base = new Database(MainActivity.this);
		baseMethods = new DatabaseMethods(base);
		baseMethods.check(R.raw.parking_database, MainActivity.this);

		parkingDatabes = new ParkingDatabase(MainActivity.this);
		baseMethods.loadAll(parkingDatabes);
		appSettings = new MySettings(vf, map, MainActivity.this, parkingDatabes);

		gantt = new Gantt(placeList, parkingDatabes, appSettings.getDistance(),
				map, listNumber, MainActivity.this);

		instructionAdapter = new InstViewAdapter(this,
				gantt.getBest(listNumber));

		listViews.getListViewInstAdapter().setAdapter(instructionAdapter);

		testPlace = new TestPlace(map);

		simulation = new SimulationThread(startDate, gantt,
				findViewById(R.id.curTimePoint),
				findViewById(R.id.textViewCurTime), MainActivity.this,
				testPlace);

		statistic = new Statistic(vf, MainActivity.this);

		intent = new Intent(MainActivity.this, MyService.class);

		startService(intent);
		servicesM = new ServiceMethods(MainActivity.this, addPlaceAdapter);
		servicesM.doBindService();

		//parkingDatabes.reservedDay();
		parkingDatabes.createTmpReservation();

	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		LinearLayout orient = (LinearLayout) findViewById(R.id.orientationStat);

		if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {

			orient.setOrientation(LinearLayout.HORIZONTAL);

		} else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
			orient.setOrientation(LinearLayout.VERTICAL);

		}
	}

	@Override
	public void onBackPressed() {

		if (vf.getDisplayedChild() == 5
				|| (vf.getDisplayedChild() == 3 && ganttViewFlipper
						.getDisplayedChild() == 0)
				|| vf.getDisplayedChild() == 6) {

			vf.setDisplayedChild(0);
			return;
		}
		if (ganttViewFlipper.getDisplayedChild() == 1) {
			ganttViewFlipper.setDisplayedChild(0);
			return;
		}

		alert.showAlertDialog3(MainActivity.this, "Uwaga",
				"Czy na pewno chcesz wyj�c?", false, geoTask);

		return;

	}

	class Gesture extends GestureDetector.SimpleOnGestureListener {

		public boolean onDoubleTap(MotionEvent ev) {

			TextView tv = (TextView) findViewById(R.id.textUserID);

			listNumber++;
			if (listNumber > 4) {
				listNumber = 0;
			}
			Toast.makeText(MainActivity.this, "" + listNumber,
					Toast.LENGTH_SHORT).show();

			ganntAdapter = new GanttViewAdapter(placeList.getList(listNumber),
					MainActivity.this, startDate, ganttViewFlipper, listNumber);

			listViews.getListViewGanttAdapter().setAdapter(ganntAdapter);

			tv.setText("Trasa dla u�ytkownika o id: " + listNumber);
			return true;
		}

	}

	protected void onDestroy() {
		try {
			servicesM.doUnbindService();
		} catch (Throwable t) {

		}
		stopService(intent);
		super.onDestroy();

	}

	public void OnResume() {

		try {
			servicesM.doBindService();
		} catch (Throwable t) {

		}
		super.onResume();
	}

	public void onPause() {
		try {
			servicesM.doUnbindService();
		} catch (Throwable t) {

		}

		super.onPause();
	}

	protected void onStop() {
		try {
			servicesM.doUnbindService();
		} catch (Throwable t) {

		}
		stopService(intent);
		super.onStop();
	}

	public void addListenerOnButtons() {

		myGestrue
				.setDoubleTap((HorizontalScrollView) findViewById(R.id.horizontalScrollView1));
		spinners.getSpinnerUsers().setOnItemSelectedListener(
				new OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> parentView,
							View selectedItemView, int position, long id) {
						placeListAdapter = new PlaceAdapter(placeList
								.getList(position), MainActivity.this);
						listViews.getListViewPlace().setAdapter(
								placeListAdapter);
						gantt.setListNumber(position);
						listNumber = position;
						instructionAdapter = new InstViewAdapter(
								MainActivity.this, gantt.getBest(position));

						listViews.getListViewGanttAdapter().setAdapter(
								ganntAdapter);

					}

					@Override
					public void onNothingSelected(AdapterView<?> parentView) {

					}
				});

		placeNameEditText.addTextChangedListener(new TextWatcher() {

			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@SuppressWarnings("unchecked")
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				addressList.clear();
				cd.checkInternet();
				List<Address> list2 = new ArrayList<Address>();
				if (!appSettings.service()) {
					geoTask = new GeocodingTask(placeNameEditText.getText()
							.toString() + " ", MainActivity.this,
							addPlaceAdapter, appSettings.getCity(),
							MainActivity.this);
					geoTask.execute(list2);
				} else {
					servicesM.sendMessageToService(appSettings.getCity(), 1);

					servicesM.sendMessageToService(placeNameEditText.getText()
							.toString(), 0);

				}
			}

			public void afterTextChanged(Editable s) {

			}
		});

		Button createButton = (Button) findViewById(R.id.button_create);

		createButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				gantt.calcualteFiveRoutes(appSettings.getUsers(),
						appSettings.isSerchForParkings(),
						appSettings.getAlghoritm());

				simulation.setUsers(appSettings.getUsers());

				menuItems.enableAll();

			}
		});

		Button addListButton = (Button) findViewById(R.id.button_add);

		addListButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				if (cd.checkInternet()) {

					vf.setDisplayedChild(2);
					spinners.getSpinnerHours().setSelection(0);
					spinners.getSpinnerMinutes().setSelection(0);

				}
			}
		});

		Button listBackButton = (Button) findViewById(R.id.button_back);

		listBackButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				hideSoftKeyboard();
				placeNameEditText.setText("");
				spinners.getSpinnerHours().setSelection(0);
				spinners.getSpinnerMinutes().setSelection(0);
				vf.setDisplayedChild(1);
				addressList.clear();

			}
		});

		Button addListButton2 = (Button) findViewById(R.id.button_list_add);

		addListButton2.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				if ((!placeNameEditText.getText().toString().isEmpty() && spinners
						.getSpinnerHours().getSelectedItemPosition() != 0)
						|| (!placeNameEditText.getText().toString().isEmpty() && spinners
								.getSpinnerMinutes().getSelectedItemPosition() != 0)) {

					Place new_place = new Place(placeNameEditText.getText()
							.toString(), spinners.getSpinnerHours()
							.getSelectedItemPosition(), (Integer) spinners
							.getSpinnerMinutes().getSelectedItem());

					if (addressList.size() != 0) {

						hideSoftKeyboard();
						new_place.getLocation().setLatitude(
								addressList.get(0).getLatitude());
						new_place.getLocation().setLongitude(
								addressList.get(0).getLongitude());
						placeListAdapter.add(new_place);
						new_place.setStartActivityDate(startDate);
						new_place.setStartDriveDate(startDate);
						ganntAdapter.notifyDataSetChanged();
						placeNameEditText.setText("");
						new_place.marker = map.addMarker(new MarkerOptions()
								.title(new_place.getName()).position(
										new LatLng(new_place.getLocation()
												.getLatitude(), new_place
												.getLocation().getLongitude())));

						vf.setDisplayedChild(1);

						addressList.clear();

					} else {
						alert.showAlertDialog(MainActivity.this, "Fail",
								"Nie znaleziono miejsca", false);
					}
				} else {

					alert.showAlertDialog(MainActivity.this, "Fail",
							"Nie wype�niono wszystkich danych", false);
				}
			}
		});

		Button setting_button = (Button) findViewById(R.id.setting_ok);
		setting_button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				vf.setDisplayedChild(0);
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater menuInflater = getMenuInflater();
		menuInflater.inflate(R.menu.menu, menu);

		menuItems.setAll(menu.findItem(R.id.menu_gantt),
				menu.findItem(R.id.menu_route),
				menu.findItem(R.id.menu_simulate));

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {

		case R.id.menu_create:

			switch (vf.getDisplayedChild()) {

			case 0:
				item.setTitle("Pokaz mape");

				vf.setDisplayedChild(1);

				return true;

			case 1:
				item.setTitle("Tworz trase");
				placeListAdapter.notifyDataSetChanged();

				vf.setDisplayedChild(0);
				return true;

			case 3:
				item.setTitle("Tworz trase");

				vf.setDisplayedChild(0);
				return true;

			default:
				return super.onOptionsItemSelected(item);

			}

		case R.id.menu_show_route:

			gantt.showParkings();
			gantt.showPlaces();
			ganntAdapter.notifyDataSetChanged();
			gantt.showRoad();
			gantt.showInstructions();
			vf.setDisplayedChild(0);

			return true;

		case R.id.menu_route_stats:

			DisplayMetrics metrics = new DisplayMetrics();
			getWindowManager().getDefaultDisplay().getMetrics(metrics);

			int width = metrics.widthPixels;
			width = (width - 54) / 5;

			statistic.createDiags(appSettings.getUsers(), width,
					gantt.getBest());

			vf.setDisplayedChild(6);
			return true;

		case R.id.menu_route_inst:
			instructionAdapter.notifyDataSetChanged();
			vf.setDisplayedChild(5);

			return true;

		case R.id.menu_gantt:

			switch (vf.getDisplayedChild()) {

			case 0:

				ganntAdapter.notifyDataSetChanged();
				vf.setDisplayedChild(3);
				return true;

			case 1:

				ganntAdapter.notifyDataSetChanged();
				vf.setDisplayedChild(3);
				return true;

			default:
				return super.onOptionsItemSelected(item);

			}

		case R.id.menu_simulate:

			if (simulation.isAlive()) {

				if (simulation.isPaused()) {
					simulation.onResume();
				} else {
					simulation.onPause();
				}

			} else {
				simulation.prepareToRun();
				simulation.start();
			}

			return true;

		case R.id.item_settings:

			switch (vf.getDisplayedChild()) {

			case 0:
				vf.setDisplayedChild(4);
				return true;

			case 1:
				vf.setDisplayedChild(4);
				return true;

			case 3:
				vf.setDisplayedChild(4);
				return true;

			default:
				return super.onOptionsItemSelected(item);

			}

		case R.id.item_show_parkings:

			Toast.makeText(MainActivity.this, "" + parkingDatabes.size(),
					Toast.LENGTH_SHORT).show();
			parkingDatabes.allMakers(map);

			return true;

		case R.id.itemClearReserv:
			parkingDatabes.clearTmpReserv();
			return true;

		case R.id.itemTmpPlaceOne:

			placeList.clearAll();
			testPlace.addONE(placeList);

			return true;

		case R.id.itemTmpPlace4:

			placeList.clearAll();
			testPlace.addALL(placeList, 4);

			return true;

		case R.id.itemTmpPlace5:

			placeList.clearAll();
			testPlace.addALL(placeList, 5);

			return true;

		case R.id.itemTmpPlace6:

			placeList.clearAll();
			testPlace.addALL(placeList, 6);

			return true;

		case R.id.itemTmpPlace7:

			placeList.clearAll();
			testPlace.addALL(placeList, 7);

			return true;

		case R.id.itemTmpPlace8:

			placeList.clearAll();
			testPlace.addALL(placeList, 8);

			return true;

		case R.id.item_add_parkings:

			Toast.makeText(MainActivity.this, "Mozna dodawac parkingi",
					Toast.LENGTH_SHORT).show();

			clicker = new MapClicker(this, parkingDatabes, map, baseMethods);

			map.setOnMapClickListener(clicker);
			map.setOnMapLongClickListener(clicker);

			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {

		super.onCreateContextMenu(menu, v, menuInfo);
		AdapterContextMenuInfo aInfo = (AdapterContextMenuInfo) menuInfo;

		Toast.makeText(this, "Item id [" + aInfo.position + "]",
				Toast.LENGTH_SHORT).show();

		int i = aInfo.position;

		menu.setHeaderTitle("Opcje dla: "
				+ placeList.get(listNumber, i).getName());

		menu.add(1, 1, 1, "Edytuj");
		menu.add(1, 2, 2, "Usu�");
		menu.add(1, 3, 3, "Poka� na mapie");
		menu.add(1, 4, 4, "Poka� najbli�sze dost�pne miejsce parkingowe");
		menu.add(1, 5, 5, "Nawiguj");
		menu.add(1, 6, 6, "Nawiguj do parkingu");

	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {

		AdapterContextMenuInfo aInfo = (AdapterContextMenuInfo) item
				.getMenuInfo();
		int i = aInfo.position;
		GoogleNavi googleNavi = new GoogleNavi();

		switch (item.getItemId()) {

		case 1:

			vf.setDisplayedChild(2);

			spinners.getSpinnerHours().setSelection(
					placeList.get(listNumber, i).getPlaceTime().getHours());
			spinners.getSpinnerMinutes().setSelection(
					placeList.get(listNumber, i).getPlaceTime().getMinutes());

			EditText place_name = (EditText) findViewById(R.id.place_name);

			place_name.setText(placeList.get(listNumber, i).getName());
			placeList.get(listNumber, i).marker.remove();
			placeList.remove(listNumber, i);
			placeListAdapter.notifyDataSetChanged();

			return true;

		case 3:

			Toast.makeText(
					this,
					""
							+ placeList.get(listNumber, i).getLocation()
									.getLatitude()
							+ ":"
							+ placeList.get(listNumber, i).getLocation()
									.getLongitude(), Toast.LENGTH_SHORT).show();

			vf.setDisplayedChild(0);

			map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
					placeList.get(listNumber, i).getLocation().getLatitude(),
					placeList.get(listNumber, i).getLocation().getLongitude()),
					16));

			return true;

		case 2:

			placeList.get(listNumber, i).marker.remove();
			placeList.remove(listNumber, i);
			placeListAdapter.notifyDataSetChanged();

			return true;

		case 4:

			parkingDatabes.search(placeList.get(listNumber, i),
					appSettings.getDistance());

			if (placeList.get(listNumber, i).isParking()) {

				vf.setDisplayedChild(0);

				map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
						placeList.get(listNumber, i).getLocation()
								.getLatitude(), placeList.get(listNumber, i)
								.getLocation().getLongitude()), 16));

				placeList.get(listNumber, i).getParkingPlace().showMarker(map);
			} else {
				Alert alert = new Alert();
				alert.showAlertDialog(
						MainActivity.this,
						"Fail",
						"Nie ma znalezionego jeszce parkingu dla tego miejsca w zadanym obr�bie",
						false);
			}
			return true;

		case 5:

			googleNavi
					.navigate(placeList.get(listNumber, i), MainActivity.this);
			return true;

		case 6:

			googleNavi.navigateToParking(placeList.get(listNumber, i),
					MainActivity.this);
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}

	}

	private void hideSoftKeyboard() {
		if (getCurrentFocus() != null && getCurrentFocus() instanceof EditText) {
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(placeNameEditText.getWindowToken(), 0);
		}
	}
}
