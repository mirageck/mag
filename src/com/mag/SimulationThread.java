package com.mag;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import place.TestPlace;

import route.Gantt;
import time.TimeMethods;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Color;
import android.view.View;
import android.widget.TextView;

public class SimulationThread extends Thread {

	private Gantt gantt;
	private TextView timePoint;
	private int users;
	private Date startDate;
	private TimeMethods myTime = new TimeMethods();
	private int startHour;
	private Date simulationDate;
	private Date stopDate;
	private TextView timeSim;
	private Activity activity;
	@SuppressLint("SimpleDateFormat")
	private SimpleDateFormat dt = new SimpleDateFormat("dd HH:mm");

	private Object lock;
	private boolean paused;

	

	TestPlace testPlace;

	public boolean isPaused() {
		return paused;
	}

	public SimulationThread(Date startDate, Gantt gantt, View findViewById,
			View view, Activity activity, TestPlace testPlace) {
		this.startDate = startDate;
		this.gantt = gantt;
		this.timePoint = (TextView) findViewById;
		this.simulationDate = startDate;
		stopDate = calculateStop(startDate);
	
		this.timeSim = (TextView) view;
		this.activity = activity;
		timePoint.setBackgroundColor(Color.WHITE);
		timePoint.setWidth(2);
		startHour = myTime.getHoursFromDate(startDate);
		timePoint.setX(myTime.calculateX(startDate, startHour));
		simulationDate = startDate;
		this.testPlace = testPlace;
	

	}

	private Date calculateStop(Date startDate2) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(startDate2);

		cal.add(Calendar.HOUR, +23);

		return cal.getTime();
	}



	public void prepareToRun() {
		simulationDate = startDate;
		lock = new Object();
		paused = false;

	}

	@Override
	public void run() {
		System.out.println("START SIMULATION " + simulationDate);
	
		do {

			synchronized (lock) {
				while (paused) {
					try {
						lock.wait();
					} catch (InterruptedException e) {
					}
				}
			}

			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			simulationDate = calculate(simulationDate);
			gantt.setSimDate(true,simulationDate);

			activity.runOnUiThread(new Runnable() {
				public void run() {

					timeSim.setText(dt.format(simulationDate));
					timePoint.setX(myTime.calculateX(simulationDate, startHour));
					
				}
			});



			for (int x = 0; x < users; x++) {
				for (int i = 0; i < gantt.best.size(x); i++) {

					// if (gantt.best.get(x, i).getTo().isParking()) {
					// if (gantt.best.get(x, i).getTo().getParkingDateEnd()
					// .before(simulationDate)) {
					// gantt.best.get(x, i).getTo().removeReservation();

					// }
					// }
					if (myTime.comparateDate(gantt.best.get(x, i).getTo()
							.getStartActivityDateEnd(), simulationDate)) {
						gantt.checkBetterParking(x, i);

					}

				}
			}

		} while (!simulationDate.equals(stopDate));
		System.out.println("STOP SIMULATION " + simulationDate);
		simulationDate = startDate;

	}

	private Date calculate(Date simulationDate2) {

		Calendar cal = Calendar.getInstance();
		cal.setTime(simulationDate2);

		cal.add(Calendar.MINUTE, +1);
		return cal.getTime();

	}

	public void setUsers(int users) {

		this.users = users;
	}

	public void onPause() {
		synchronized (lock) {
			paused = true;
		}
	}

	public void onResume() {
		synchronized (lock) {
			paused = false;
			lock.notifyAll();
		}
	}

}
