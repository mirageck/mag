package com.mag;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import route.BestList;
import android.content.Context;
import android.graphics.Color;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;

public class Statistic {

	private List<Integer> timeListH = new ArrayList<Integer>();
	private List<Integer> timeListMin = new ArrayList<Integer>();
	private List<Double> distanceList = new ArrayList<Double>();
	private List<Long> durationList = new ArrayList<Long>();

	private Context context;

	LinearLayout linearChart;
	LinearLayout linearChart2;
	LinearLayout linearChart3;

	public Statistic(ViewFlipper vf, Context context) {

		this.context = context;

		linearChart = (LinearLayout) vf.findViewById(R.id.distanceStat);
		linearChart2 = (LinearLayout) vf.findViewById(R.id.timeStat);
		linearChart3 = (LinearLayout) vf.findViewById(R.id.durationStat);

	}

	public void setList() {

	}

	private void createStats(int size, BestList list) {

		for (int i = 0; i < size; i++) {

			double sum = 0;
			int time = 0;

			for (int j = 0; j < list.size(i); j++) {
				sum = (sum + list.get(i, j).getDistance());

			}

			long diff = list.getEndDate(i).getTime()
					- list.getStartDate(i).getTime();

			long minutes = TimeUnit.MILLISECONDS.toMinutes(diff);

			time = (int) minutes;

			sum *= 100;
			sum = Math.round(sum);
			sum /= 100;

			distanceList.add(sum);
			int timeH = time / 60;
			int timeMin = time - timeH * 60;
			timeListH.add(timeH);
			timeListMin.add(timeMin);
			durationList.add(list.getTimes(i));

		}

	}

	public Integer getTimeMin(int index) {
		return timeListMin.get(index);

	}

	public Integer getTimeH(int index) {
		return timeListH.get(index);

	}

	public Double getDistanceList(int index) {
		return distanceList.get(index);

	}

	public List<Long> getDurationList() {
		return durationList;
	}

	public void setDurationList(List<Long> durationList) {
		this.durationList = durationList;
	}

	public void createDiags(int size, int width, BestList list) {

		createStats(size, list);

		 linearChart.removeAllViews();
		 linearChart2.removeAllViews();
		 linearChart3.removeAllViews();

		for (int x = 0; x < size; x++) {

			int tmp = (int) (distanceList.get(x) * 20);

			LinearLayout view = new LinearLayout(context);
			view.setBackgroundColor(Color.GRAY);
			view.setLayoutParams(new LinearLayout.LayoutParams(width, tmp));
			LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) view
					.getLayoutParams();
			params.setMargins(3, 0, 3, 0);

			view.setLayoutParams(params);

			TextView txt = new TextView(context);

			txt.setText(distanceList.get(x) + " km");
			txt.setTextColor(Color.BLACK);

			view.addView(txt);

			linearChart.addView(view);
		}

		for (int x = 0; x < size; x++) {

			int tmp = (timeListH.get(x) * 60) + timeListMin.get(x) * 5;

			LinearLayout view = new LinearLayout(context);

			view.setBackgroundColor(Color.RED);
			view.setLayoutParams(new LinearLayout.LayoutParams(width, tmp));
			LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) view
					.getLayoutParams();
			params.setMargins(3, 0, 3, 0);

			view.setLayoutParams(params);

			TextView txt = new TextView(context);

			txt.setText(timeListH.get(x) + "h" + timeListMin.get(x) + "min");
			txt.setTextColor(Color.BLACK);

			view.addView(txt);

			linearChart2.addView(view);
		}

		for (int x = 0; x < size; x++) {

			int tmp = (int) (durationList.get(x) * 20);

			LinearLayout view = new LinearLayout(context);
			view.setBackgroundColor(Color.YELLOW);

			view.setLayoutParams(new LinearLayout.LayoutParams(width, tmp));
			LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) view
					.getLayoutParams();
			params.setMargins(3, 0, 3, 0);

			view.setLayoutParams(params);

			TextView txt = new TextView(context);

			txt.setText(durationList.get(x) + "s");
			txt.setTextColor(Color.BLACK);

			view.addView(txt);

			linearChart3.addView(view);
		}

		System.out.println("KONIEC");

	}

}
