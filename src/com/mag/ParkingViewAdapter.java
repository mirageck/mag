package com.mag;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import time.MyDate;
import utility.Alert;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ParkingViewAdapter extends ArrayAdapter<MyDate> {

	private List<MyDate> reservationList;
	private Context context;

	private int startHour;

	private LinearLayout hoursLayout;
	// private LinearLayout parkingLayout;

	private SimpleDateFormat dt;

	@SuppressLint("SimpleDateFormat")
	public ParkingViewAdapter(List<MyDate> reservationList,
			Context ctx, int startHour, LinearLayout hoursLayout) {
		super(ctx, R.layout.row_layout, reservationList);
		this.reservationList = reservationList;
		this.context = ctx;
		this.hoursLayout = hoursLayout;

		this.startHour = startHour;
		dt = new SimpleDateFormat("dd-MMM HH:mm a");
		createHoursLayout();
	}

	public void createHoursLayout() {

		TextView hour;
		// Button hour;

		int tmp = startHour;

		for (int i = 0; i < 24; i++) {
			hour = new TextView(context);
			// hour = new Button(context);
			hour.setText("" + tmp + ":00");
			tmp++;
			if (tmp > 24) {
				tmp = tmp - 24;
			}
			hour.setId(i);
			hour.setWidth(120);
			hour.setBackgroundResource(R.drawable.tlo);
			hoursLayout.addView(hour);
		}

	}

	public View getView(int position, View rowView, ViewGroup parent) {

		final MyDate p = reservationList.get(position);

		if (rowView == null) {

			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			rowView = inflater.inflate(R.layout.row_parking_layout, parent,
					false);

		}

		TextView tv = (TextView) rowView.findViewById(R.id.textParkingGantt);
		TextView reservationView = (TextView) rowView
				.findViewById(R.id.reservationTextView);

		tv.setBackgroundColor(Color.WHITE);
		tv.setTextColor(Color.BLACK);
		reservationView.setBackgroundColor(Color.WHITE);
		reservationView.setTextColor(Color.BLACK);

		tv.setText(p.getName());

		// wyliczanie wielkosci aktywnosci i lokalizacji na wykresie
		String tmp = getStringTime(p.getH(), p.getMin());
		reservationView.setText(tmp);

		int size = (p.getH() * 60) + p.getMin();
		size = size * 2;

		reservationView.setWidth(size);

		int loc = calculateX(p.getTimeStart());

		reservationView.setX(loc);

		reservationView.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String tmp = getStringTime(p.getH(), p.getMin());
				Alert alert = new Alert();
				alert.showAlertDialog(
						context,
						"Rezerwacja",
						"Czas rozpoczecia: " + dt.format(p.getTimeStart())
								+ "\n" + "Czas trwania: " + tmp + "\n"
								+ "Czas zakończenia: "
								+ dt.format(p.getTimeEnd()), false);

			}
		});

		return rowView;

	}

	public String getStringTime(int hours, int mins) {
		String tmp = "";

		if (hours != 0) {

			tmp = tmp + hours + "h ";
		}
		if (mins != 0) {

			tmp = tmp + mins + "min ";
		}
		return tmp;
	}

	public int calculateX(Date date) {
		int tmpHours = getHoursFromDate(date);
		int tmpMin = getMinutesFromDate(date);

		tmpHours = tmpHours - startHour;

		if (tmpHours < 0) {
			// tmpHours = 24 - tmpHours;
			int tmp = 24 - startHour;
			tmpHours = getHoursFromDate(date) + tmp;
		}

		int loc2 = (tmpHours * 60) + (tmpMin);
		loc2 = loc2 * 2;
		return loc2;
	}

	public int getHoursFromDate(Date date) {
		Calendar calendar = GregorianCalendar.getInstance();

		calendar.setTime(date);

		return calendar.get(Calendar.HOUR_OF_DAY);
	}

	public int getMinutesFromDate(Date date) {
		Calendar calendar = GregorianCalendar.getInstance(); // creates a

		calendar.setTime(date);
		return calendar.get(Calendar.MINUTE);
	}

}
