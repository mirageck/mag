package com.mag;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.model.Marker;

public class CustomInfoAdapter implements InfoWindowAdapter {

	Context context;


	public CustomInfoAdapter(Context tmp) {
		// TODO Auto-generated constructor stub
		context = tmp;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getInfoContents(Marker marker) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View v = inflater.inflate(R.layout.marker_info, null);

		TextView name = (TextView) v.findViewById(R.id.marker_name);

		TextView description = (TextView) v
				.findViewById(R.id.marker_reservation);

		//LinearLayout layout = (LinearLayout) v.findViewById(R.id.marker_layout);


		name.setText(marker.getTitle());

		description.setText(marker.getSnippet());

		return v;
	}

	@Override
	public View getInfoWindow(Marker marker) {


		return null;
	}

}
