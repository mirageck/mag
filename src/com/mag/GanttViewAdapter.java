package com.mag;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.mag.R;

import place.Place;
import time.TimeMethods;
import utility.Alert;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ViewFlipper;

public class GanttViewAdapter extends ArrayAdapter<Place> {

	private List<Place> placeList;
	private Context context;

	private int startHour;

	private LinearLayout hoursLayout;
	private LinearLayout hoursLayout2;
	private ListView listViewReservAdpater;
	ViewFlipper ganttViewFlipper;
	int listNumber;
	private TimeMethods myTime = new TimeMethods();

	private SimpleDateFormat dt;

	@SuppressLint("SimpleDateFormat")
	public GanttViewAdapter(List<Place> placeList, Context ctx, Date startDate,
			ViewFlipper ganttViewFlipper, int listNumber) {
		super(ctx, R.layout.row_layout, placeList);
		this.placeList = placeList;
		this.context = ctx;
		this.listNumber = listNumber;

		this.ganttViewFlipper = ganttViewFlipper;

		this.listViewReservAdpater = (ListView) ganttViewFlipper
				.findViewById(R.id.ganttReservationAdapterList);

		this.hoursLayout = (LinearLayout) ganttViewFlipper
				.findViewById(R.id.ganttHours);
		this.hoursLayout2 = (LinearLayout) ganttViewFlipper
				.findViewById(R.id.ganttReservationHours);

		startHour = myTime.getHoursFromDate(startDate);
		dt = new SimpleDateFormat("dd-MMM HH:mm a");
		createHoursLayout();
	}

	public void createHoursLayout() {

		if (hoursLayout.getChildCount() == 0) {
			TextView hour;
			// Button hour;

			int tmp = startHour;

			for (int i = 0; i < 24; i++) {
				hour = new TextView(context);
				// hour = new Button(context);
				hour.setText("" + tmp + ":00");
				tmp++;
				if (tmp > 24) {
					tmp = tmp - 24;
				}
				hour.setId(i);
				hour.setWidth(120);
				hour.setBackgroundResource(R.drawable.tlo);
				hoursLayout.addView(hour);
			}
		}
	}

	public View getView(int position, View rowGantttView, ViewGroup parent) {

		final Place p = placeList.get(position);

		if (rowGantttView == null) {

			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			rowGantttView = inflater.inflate(R.layout.row_gantt_layout, parent,
					false);

		}

		TextView tv = (TextView) rowGantttView
				.findViewById(R.id.textGantLayout1);
		TextView activityView = (TextView) rowGantttView
				.findViewById(R.id.activityTextView);
		TextView driveView = (TextView) rowGantttView
				.findViewById(R.id.driveTextView);

		TextView parkingView = (TextView) rowGantttView
				.findViewById(R.id.parkingTextView);

		tv.setBackgroundColor(Color.WHITE);
		tv.setTextColor(Color.BLACK);
		activityView.setBackgroundColor(Color.WHITE);
		activityView.setTextColor(Color.BLACK);
		driveView.setBackgroundColor(Color.RED);
		tv.setText(p.getName());

		// wyliczanie wielkosci aktywnosci i lokalizacji na wykresie
		String tmp = getStringTime(p.getPlaceTime().getHours(), p.getPlaceTime().getMinutes());
		activityView.setText(tmp);

		int size = (p.getPlaceTime().getHours() * 60) + p.getPlaceTime().getMinutes();
		size = size * 2;

		activityView.setWidth(size);

		int loc = myTime.calculateX(p.getStartActivityDate(), startHour);

		activityView.setX(loc);

		int roadHours = p.getDriveTime().getHours();
		int roadMinutes = p.getDriveTime().getMinutes();

		int size2 = (roadHours * 60) + (roadMinutes);
		size2 = size2 * 2;
		driveView.setWidth(size2);

		int loc2 = myTime.calculateX(p.getStartDriveDate(), startHour);

		driveView.setX(loc2);

		if (p.isParking()) {

			int locP = myTime.calculateX(p.getParkingDate(), startHour);

			parkingView.setWidth(size);
			parkingView.setX(locP);
			parkingView.setBackgroundColor(Color.BLUE);

		}

		driveView.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				String tmp = getStringTime(p.getDriveTime().getHours(),
						p.getDriveTime().getMinutes());
				Alert alert = new Alert();
				alert.showAlertDialog(context, "Dojazd", "Czas rozpoczecia:"
						+ dt.format(p.getStartDriveDate()) + "\n"
						+ "Czas trwania: " + tmp, false);

			}
		});

		activityView.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String tmp = getStringTime(p.getPlaceTime().getHours(), p.getPlaceTime().getMinutes());
				Alert alert = new Alert();
				alert.showAlertDialog(
						context,
						"Przystanek",
						"Czas rozpoczecia: "
								+ dt.format(p.getStartActivityDate()) + "\n"
								+ "Czas trwania: " + tmp, false);

			}
		});

		parkingView.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String tmp = getStringTime(p.getPlaceTime().getHours(), p.getPlaceTime().getMinutes());
				Alert alert = new Alert();
				alert.showAlertDialog(context, "Parking", "Czas rozpoczecia: "
						+ dt.format(p.getParkingDate()) + "\n"
						+ "Czas trwania: " + tmp + "\n"
						+ "Odleg�o�� od miejsca: " + p.getDistanceToParking()
						+ "\n" + p.getParkingCorectionMinutes(), false);

			}
		});

		parkingView.setOnLongClickListener(new View.OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				// ClipData data = ClipData.newPlainText("", "");

				// Instantiates the drag shadow builder.

				// DragShadowBuilder shadowBuilder = new
				// View.DragShadowBuilder(v);

				// Starts the drag
				// v.startDrag(data, // the data to be dragged
				// shadowBuilder, // the drag shadow builder
				// null, // no need to use local data
				// 0 // flags (not currently used, set to 0)
				// );

				ParkingViewAdapter parkingAdapter = new ParkingViewAdapter(p
						.getParkingPlace().getReservationList(), context,
						startHour, hoursLayout2);

				listViewReservAdpater.setAdapter(parkingAdapter);

				ganttViewFlipper.showNext();

				return true;
			}
		});

		return rowGantttView;

	}

	public String getStringTime(int hours, int mins) {
		String tmp = "";

		if (hours != 0) {

			tmp = tmp + hours + "h ";
		}
		if (mins != 0) {

			tmp = tmp + mins + "min ";
		}
		return tmp;
	}

}
