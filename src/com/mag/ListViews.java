package com.mag;

import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ListView;

public class ListViews {

	private ListView listViewPlace;
	private ListView listViewAddPlace;
	private ListView listViewGanttAdapter;
	private ExpandableListView listViewInstAdapter;
	
	public ListView getListViewPlace() {
		return listViewPlace;
	}

	public void setListViewPlace(ListView listViewPlace) {
		this.listViewPlace = listViewPlace;
	}

	public ListView getListViewAddPlace() {
		return listViewAddPlace;
	}

	public void setListViewAddPlace(ListView listViewAddPlace) {
		this.listViewAddPlace = listViewAddPlace;
	}

	public ListView getListViewGanttAdapter() {
		return listViewGanttAdapter;
	}

	public void setListViewGanttAdapter(ListView listViewGanttAdapter) {
		this.listViewGanttAdapter = listViewGanttAdapter;
	}

	public ListViews(View idPlace, View idAddPlace, View idGantt, View idInst) {
		// TODO Auto-generated constructor stub
		
		listViewPlace = (ListView) idPlace;
		listViewAddPlace = (ListView) idAddPlace;
		listViewGanttAdapter = (ListView) idGantt;
		setListViewInstAdapter((ExpandableListView) idInst);
		
	}

	public ExpandableListView getListViewInstAdapter() {
		return listViewInstAdapter;
	}

	public void setListViewInstAdapter(ExpandableListView listViewInstAdapter) {
		this.listViewInstAdapter = listViewInstAdapter;
	}
	

}
