package com.mag;



import parking.ParkingDatabase;
import parking.ParkingLocation;
import android.annotation.SuppressLint;
import android.content.Context;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mag.R;

import database.DatabaseMethods;

@SuppressLint("SdCardPath")
public class MapClicker implements OnMapClickListener, OnMapLongClickListener {

	GoogleMap map;
	Context contex;
	ParkingDatabase parking_databes;
	DatabaseMethods baseMethods;

	public MapClicker(Context c, ParkingDatabase parking_databes,
			GoogleMap map, DatabaseMethods baseMethods) {
		contex = c;
		this.parking_databes = parking_databes;
		this.map = map;
		this.baseMethods = baseMethods;

	}

	@Override
	public void onMapClick(LatLng point) {

	}

	@SuppressLint("SdCardPath")
	@Override
	public void onMapLongClick(LatLng point) {

		baseMethods.addRecord(point.latitude, point.longitude);

		Marker marker = map.addMarker(new MarkerOptions()
				.icon(BitmapDescriptorFactory.fromResource(R.drawable.parking))
				.anchor(0.0f, 1.0f)
				.position(new LatLng(point.latitude, point.longitude)));

		marker.setVisible(true);

		ParkingLocation tmp = new ParkingLocation(point.latitude,
				point.longitude);

		parking_databes.addParkingLocation(tmp);
		Toast.makeText(contex, "" + point.toString(), Toast.LENGTH_SHORT)
				.show();

	}
}
